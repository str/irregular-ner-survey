{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2c9c3d2e-3d34-4f45-ae29-b380584da74c",
   "metadata": {},
   "source": [
    "# Train-Test Folds\n",
    "\n",
    "To ensure replicability across trials, I will maintain the same train-val-test splits across trials, using a 10-fold cross-validation scheme for obtaining performance results.\n",
    "\n",
    "I will prepare two sets of folds: splits in the first set will be at the \"doc\" level (i.e. whole posts on the CADEC dataset), while in the second set I will use the \"sentence\" level. For the stratification, I will use the \"continuity_type\" attribute that describes whether a specific entity is continuous, discontinuous or overlapping with other entities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "4d0cce2f-997c-4067-8ee0-4bdf7595c154",
   "metadata": {},
   "outputs": [],
   "source": [
    "from os import path\n",
    "import pandas as pd\n",
    "\n",
    "DATA_DIR = \"../data/standardized\"\n",
    "DATASET_NAME = \"psytar\"\n",
    "SAMPLE_NAME = \"psytar_full\"\n",
    "\n",
    "entity_attributes = pd.read_pickle(path.join(DATA_DIR, DATASET_NAME, \"entity_attributes_individual.pkl\"))\n",
    "comprises = pd.read_pickle(path.join(DATA_DIR, DATASET_NAME, \"comprises.pkl\"))\n",
    "words = pd.read_pickle(path.join(DATA_DIR, DATASET_NAME, \"words.pkl\"))  # Contains the sentence IDs\n",
    "\n",
    "# Merge dataframes to get \"continuity_type\" for doc IDs, as well as sentence IDs\n",
    "merged = words.merge(comprises, on=[\"doc_id\", \"word_id\"], how=\"left\") \\\n",
    "    .merge(entity_attributes, on=[\"doc_id\", \"entity_id\"], how=\"left\") \\\n",
    "    .drop(columns=[\"word_id\", \"word_text\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4371a2cd-3b6d-475b-b75e-9abdcfe572ad",
   "metadata": {},
   "source": [
    "I will assign a class to each \"doc\" or \"sentence\" as follows:\n",
    "\n",
    "* Every \"doc\" or \"sentence\" that contains at least one discontinuous entity will be marked with \"D\". Discontinuous entities are quite rare (in the CADEC dataset), so I am giving this characteristic priority over the presence of overlapping or discontinuous entities.\n",
    "* Remaining passages that contain at least one set of overlapping entities will be marked with \"O\", and\n",
    "* Out of the other passages, those containing at least one continuous entity will be marked with \"C\", and\n",
    "* Finally, all other passages will be marked with \"N\", for \"None\".\n",
    "\n",
    "I will use this characteristic to produce stratified train-test-splits for each fold."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "a5dd2982-6018-4158-b2c6-0a64c3666407",
   "metadata": {},
   "outputs": [],
   "source": [
    "def assign_continuity_class(s: pd.Series) -> str:\n",
    "    \"\"\"Returns the continuity class for a series of words or entities.\n",
    "    In practice, it is used over all words in a doc or a sentence.\n",
    "    \"\"\"\n",
    "    ls = s.tolist()\n",
    "    discontinuity_tags = [e[0] for e in ls]\n",
    "    overlap_locations = [e[1] for e in ls]\n",
    "    if (True, \"none\") in ls:  # if entity is discontinuous and non-overlapping\n",
    "        return \"D\"\n",
    "    elif len([t for t in overlap_locations if t != \"none\" and t == t]):\n",
    "        return \"O\"\n",
    "    elif False in discontinuity_tags:\n",
    "        return \"C\"\n",
    "    else:\n",
    "        return \"N\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "19f510e3-fc8f-4282-a261-6ecb99703a39",
   "metadata": {},
   "outputs": [],
   "source": [
    "merged[\"continuity_type\"] = list(zip(merged[\"discontinuity\"], merged[\"overlap_location\"]))\n",
    "docs_with_class = merged.groupby(\"doc_id\")[\"continuity_type\"].apply(assign_continuity_class)\n",
    "sents_with_class = merged.groupby([\"sentence_id\"])[\"continuity_type\"].apply(assign_continuity_class)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af5550e3-32a1-48ba-8dce-3311fb627cf5",
   "metadata": {},
   "source": [
    "For the k-fold cross-validation, I am shuffling the data first and then splitting them into 10 subsets. At each fold, I am using the first 6 subsets as training data, while the 7th/8th subsets are set aside for validation (during training epochs) and the 9th/10th subsets for testing (for a train-val-test ratio of 60-20-20)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "98bf0f3d-1e47-4f73-81cd-e33e929ce36c",
   "metadata": {},
   "outputs": [],
   "source": [
    "NUM_FOLDS = 10\n",
    "RANDOM_STATE = 0\n",
    "\n",
    "from sklearn.model_selection import StratifiedKFold\n",
    "\n",
    "def generate_folds(data):\n",
    "    folds = {}\n",
    "    \n",
    "    # Produce as many subsets as the number of folds\n",
    "    subsets = []\n",
    "    \n",
    "    skf = StratifiedKFold(n_splits=NUM_FOLDS, shuffle=True, random_state=RANDOM_STATE)\n",
    "    for _, subset_indices in skf.split(data.index, data.values):\n",
    "        # Map subset indices to passage IDs\n",
    "        subset = data[subset_indices].index.tolist()\n",
    "        subsets.append(subset)\n",
    "    \n",
    "    # At each fold, use one subset for validation and one for testing\n",
    "    for fold in range(NUM_FOLDS):\n",
    "        train_subsets = [(fold + i) % NUM_FOLDS for i in range(NUM_FOLDS-4)]\n",
    "        train_set = [e for s in train_subsets for e in subsets[s]]\n",
    "        \n",
    "        val_subsets = [(fold + i) % NUM_FOLDS for i in [NUM_FOLDS-4, NUM_FOLDS-3]]\n",
    "        val_set = [e for s in val_subsets for e in subsets[s]]\n",
    "                \n",
    "        test_subsets = [(fold + i) % NUM_FOLDS for i in [NUM_FOLDS-2, NUM_FOLDS-1]]\n",
    "        test_set = [e for s in test_subsets for e in subsets[s]]\n",
    "\n",
    "        folds[fold] = {\n",
    "            \"train\": train_set,\n",
    "            \"val\": val_set,\n",
    "            \"test\": test_set\n",
    "        }\n",
    "    \n",
    "    return folds\n",
    "\n",
    "\n",
    "doc_folds = generate_folds(docs_with_class)\n",
    "sent_folds = generate_folds(sents_with_class)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0dc95ca-6540-461b-ab8f-882f3edc1078",
   "metadata": {},
   "source": [
    "In order to avoid overwriting previously generated folds by mistake, overwriting stored files is blocked in this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "7a55cb05-cc01-42fb-925f-ebc5a8500439",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pickle\n",
    "from os import makedirs\n",
    "\n",
    "FOLD_SAVE_DIR = \"../data/folds\"\n",
    "\n",
    "save_path = path.join(FOLD_SAVE_DIR, SAMPLE_NAME)\n",
    "if not path.exists(save_path):\n",
    "    makedirs(save_path)\n",
    "    \n",
    "with open(path.join(save_path, \"docs.pkl\"), \"xb\") as f:\n",
    "    pickle.dump(doc_folds, f)\n",
    "\n",
    "with open(path.join(save_path, \"sentences.pkl\"), \"xb\") as f:\n",
    "    pickle.dump(sent_folds, f)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python (lit-repl-dirkson21)",
   "language": "python",
   "name": "lit-repl-dirkson21"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
