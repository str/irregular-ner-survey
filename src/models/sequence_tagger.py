import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa
from transformers import TFAutoModel, AutoConfig


class Mask(tf.keras.layers.Layer):  
    def call(self, inputs):
        return inputs[0]
        
    def compute_mask(self, inputs, mask=None):
        return inputs[1]
    
    def get_config(self):
        return super(Mask, self).get_config()
    
    @classmethod
    def from_config(cls, config):
        self()


class SequenceTaggingModel(tf.keras.Model):
    def __init__(self,
                 num_tags,
                 bert_config,
                 initialized_word_embeddings,
                 char_vocab_len,
                 max_seq_len=256,
                 char_max_len=25,
                 char_embedding_size=25,
                 char_lstm_size=25,
                 dropout_rate=0.1,
                 lstm_hidden_size=200
                ):
        super(SequenceTaggingModel, self).__init__()
        self.num_tags = num_tags
        self.bert_config = bert_config
        self.initialized_word_embeddings = initialized_word_embeddings
        self.char_vocab_len = char_vocab_len
        self.max_seq_len = max_seq_len
        self.char_max_len = char_max_len
        self.char_embedding_size = char_embedding_size
        self.char_lstm_size = char_lstm_size
        self.dropout_rate = dropout_rate,
        self.lstm_hidden_size = lstm_hidden_size
        
        # WORD REPRESENTATION
        # Bert model output
        self.bert_model = TFAutoModel.from_config(bert_config).layers[0]
        
        # GloVe/FastText embeddings
        self.word_embeddings = tf.keras.layers.Embedding(
            input_dim=initialized_word_embeddings.shape[0],
            output_dim=initialized_word_embeddings.shape[1],
            weights=[initialized_word_embeddings],
            input_length=max_seq_len,
            mask_zero=False,
            name="word_embeddings"
        )
        
        # Char embeddings are learned through a BiLSTM
        self.char_embeddings = tf.keras.layers.Embedding(
            input_dim=char_vocab_len,
            output_dim=char_embedding_size,
            mask_zero=False,
            name="char_embeddings"
        )
        self.char_lstm = tf.keras.layers.TimeDistributed(
            tf.keras.layers.Bidirectional(
                tf.keras.layers.LSTM(units=char_lstm_size)
            ),
            name="char_lstm"
        )
        
        # Concatenate embeddings
        self.concat_embeddings = tf.keras.layers.Concatenate(name="embeddings")
        
        # Apply dropout to embedding output
        self.dropout = tf.keras.layers.Dropout(rate=dropout_rate, name="dropout")
        
        # Apply mask for all special tokens for use with the following layers and loss function
        self.mask = Mask(name="mask")
        
        # Add a BiLSTM layer
        self.bilstm = tf.keras.layers.Bidirectional(
            tf.keras.layers.LSTM(units=lstm_hidden_size, 
                                 return_sequences=True),
            name="bilstm"
        )
        #x = tf.keras.layers.Dense(fc_dim, activation="tanh")(x)
        
        # Add a CRF layer
        self.crf = tfa.layers.crf.CRF(units=num_tags, name="crf")
        
        # Build model
        input_types = {
            "bert_input_ids": dict(shape=(self.max_seq_len,), 
                                   dtype="int32"),
            "bert_mask": dict(shape=(self.max_seq_len,), 
                              dtype="int32"),
            "word_ids": dict(shape=(self.max_seq_len,), 
                              dtype="int32"),
            "char_ids": dict(shape=(self.max_seq_len, 
                                    self.char_max_len,),
                             dtype="int32"),
            "mask": dict(shape=(self.max_seq_len,), 
                         dtype="bool")
        }
        init_inputs = [tf.ones(shape=(1,) + input_types[i]["shape"],
                                dtype=input_types[i]["dtype"])
                       for i in ["bert_input_ids",
                                 "bert_mask",
                                 "word_ids",
                                 "char_ids",
                                 "mask"
                                ]]
        self(init_inputs)
        
    def get_optimizer_groups(self):        
        return self.bert_model, [self.word_embeddings, self.char_embeddings, 
                                 self.char_lstm, self.bilstm, self.crf]
        
    def call(self, inputs, return_crf_internal=False):
        bert_input_ids, bert_mask, word_ids, char_ids, mask = inputs
        
        bert_embeddings = self.bert_model(bert_input_ids, bert_mask).last_hidden_state
        word_embeddings = self.word_embeddings(word_ids)
        char_embeddings = self.char_embeddings(char_ids)
        char_lstm = self.char_lstm(char_embeddings)
        
        x = self.concat_embeddings([bert_embeddings,
                                    word_embeddings,
                                    char_lstm])
        x = self.dropout(x)
        x = self.mask([x, mask])
        
        x = self.bilstm(x)
        
        decoded_sequence, potentials, sequence_length, kernel = self.crf(x, mask=bert_mask)
        
        if return_crf_internal:
            return (potentials, sequence_length, kernel), decoded_sequence
        else:
            return decoded_sequence
    
    def compute_crf_loss(self, potentials, sequence_length, kernel, y):
        crf_likelihood, _ = tfa.text.crf_log_likelihood(potentials, 
                                                        y, 
                                                        sequence_length, 
                                                        kernel)
        return tf.reduce_mean(-1 * crf_likelihood)
    
    def train_step(self, data):
        x, y = data
        
        with tf.GradientTape() as tape:
            (potentials, sequence_length, kernel), decoded_sequence, *_ = self(
                x, return_crf_internal=True
            )
            crf_loss = self.compute_crf_loss(potentials, sequence_length, 
                                             kernel, y)
            loss = crf_loss + tf.reduce_sum(self.losses)
            
        # Compute gradients            
        gradients = tape.gradient(loss, self.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))

        # Update metrics (includes the metric that tracks the loss)
        self.compiled_metrics.update_state(y, decoded_sequence)
        
        # Return a dict mapping metric names to current value
        orig_results = {m.name: m.result() for m in self.metrics}
        crf_results = {"loss": loss, "crf_loss": crf_loss}
        return {**orig_results, **crf_results}
    
    def test_step(self, data):
        x, y = data
        (potentials, sequence_length, kernel), decode_sequence, *_ = self(
            x, return_crf_internal=True
        )
        crf_loss = self.compute_crf_loss(potentials, sequence_length, 
                                         kernel, y)
        loss = crf_loss + tf.reduce_sum(self.losses)
        
        # Update metrics (includes the metric that tracks the loss)
        self.compiled_metrics.update_state(y, decode_sequence)
        
        # Return a dict mapping metric names to current value
        results = {m.name: m.result() for m in self.metrics}
        results.update({"loss": loss, "crf_loss": crf_loss})  # append loss
        return results

    def get_config(self):
        return {"num_tags": self.num_tags,
                "bert_model_name": self.bert_config._name_or_path,
                "initialized_word_embeddings": self.initialized_word_embeddings,
                "char_vocab_len": self.char_vocab_len,
                "max_seq_len": self.max_seq_len,
                "char_max_len": self.char_max_len,
                "char_embedding_size": self.char_embedding_size,
                "char_lstm_size": self.char_lstm_size,
                "dropout_rate": self.dropout_rate,
                "lstm_hidden_size": self.lstm_hidden_size
               }
    
    @classmethod
    def from_config(cls, config):
        bert_name = config.pop("bert_model_name")
        bert_config = AutoConfig.from_pretrained(bert_name)
        initialized_word_embeddings = np.array(config.pop("initialized_word_embeddings"))
    
        return cls(bert_config=bert_config,
                   initialized_word_embeddings=initialized_word_embeddings,
                   **config)
        
    def summary(self):
        # Input sentences are padded to the model's maximum length
        bert_input_ids = tf.keras.Input(shape=(self.max_seq_len,), 
                                        dtype="int32", 
                                        name="bert_input_ids")
        bert_mask = tf.keras.Input(shape=(self.max_seq_len,), 
                                          dtype="int32", name="bert_mask")
        word_ids = tf.keras.Input(shape=(self.max_seq_len,),
                                  dtype="int32", name="word_ids")
        char_ids = tf.keras.Input(shape=(self.max_seq_len, 
                                         self.char_max_len,),
                                  dtype="int32", name="char_ids")
        mask = tf.keras.Input(shape=(self.max_seq_len,), 
                              dtype="bool", name="input_mask")
        inputs = [bert_input_ids, bert_mask, word_ids, char_ids, mask]
        return tf.keras.Model(inputs, self(inputs)).summary()   
    