import os
import itertools

from stanza.server import CoreNLPClient
import pandas as pd
from tqdm import tqdm

CORENLP_HOME = "/data/s3386473/stanza_corenlp"


def generate_li_input_entries(dai_input):
    li_input = pd.DataFrame(index=dai_input.index)
    li_input["doc_key"] = li_input.index.get_level_values("sentence_id")
    li_input["sentences"] = dai_input["sentence"].str.split(" ").apply(lambda x: [x])
    
    print(".. Converting boundaries to continuous chunks and relations...")
    li_input[["ner", "relations"]] = dai_input["boundaries"] \
        .apply(lambda x: pd.Series([[r] for r in convert_boundaries_to_chunks_relations(x)], index=["ner", "relations"]))
    
    print(".. Computing dependencies...")
    os.environ["CORENLP_HOME"] = CORENLP_HOME
    deps = []
    with CoreNLPClient(memory='8g', timeout=60000, be_quiet=True, annotators=["depparse"], 
                   properties={ "tokenize.whitespace": True }) as corenlp:
        for s in tqdm(dai_input["sentence"].values):  # Iteration in order to be able to check progress
            deps.append([{"nodes": compute_dependencies(s, corenlp)}])
    li_input["dep"] = deps
    
    return li_input

        
def convert_boundaries_to_chunks_relations(boundaries):
    chunks = []
    relations = []
    
    if boundaries == boundaries:
        # Make sure to include unique spans
        for e in boundaries:
            for s in e:
                c = [s[0], s[1], "ADR"]
                if c not in chunks:
                    chunks.append(c)

        # Discontinuous entities
        relations.extend([[s1[0], s1[1], s2[0], s2[1], "Combined"] for e in boundaries for i1, s1 in enumerate(e) for i2, s2 in enumerate(e) if i1 != i2])

        # Overlapping components
        for ei1, e1 in enumerate(boundaries):
            for ei2, e2 in enumerate(boundaries):
                for i1, s1 in enumerate(e1):
                    s1_tokens = range(s1[0], s1[1]+1)
                    for i2, s2 in enumerate(e2):
                        s2_tokens = range(s2[0], s2[1]+1)
                        if ei1 != ei2 and not(s1[0] == s2[0] and s1[1] == s2[1]):
                            if len(set(s1_tokens) & set(s2_tokens)) > 0:
                                relations.append([s1[0], s1[1], s2[0], s2[1], "Overlap"])
                            
    return chunks, relations


def convert_chunks_relations_to_boundaries(components, relations):
    components = [[(c[0], c[1]), c[2]] for c in components]
    relations = [[((r[0], r[1]), (r[2], r[3])), r[4]] for r in relations]
    
    entities = []
    for component, _ in components:
        # Merge all related components into 
        component_set = set()
        component_set.add(component)
        for (rcomponent_1, rcomponent_2), relation in relations:
            if relation != 'Combined':
                continue
            if component == rcomponent_1:
                component_set.add(rcomponent_2)
            if component == rcomponent_2:
                component_set.add(rcomponent_1)
                
        # Attempt to evaluate if the whole set is an entity
        # If not, check its subsets with length len(set)-1
        # Do NOT consider single-component subsets, as for these there would be
        # no relation in the first place
        num_decrements = 0
        entities_have_been_found = False
        while num_decrements <= 1 and not entities_have_been_found:
            candidate_length = len(component_set) - num_decrements
            for candidate in __find_subsets(component_set, 
                                            candidate_length):
                if candidate not in entities:
                    if __is_clique(candidate, relations):
                        entities.append(candidate)
                        entities_have_been_found = True
            num_decrements += 1
            
            # Do not check single-chunk subsets
            # (if a chunk had no relation to others, it will be evaluated before this check)
            if candidate_length <= 1:
                break
                
    sorted_entities = [list(e) for e in entities]
    [e.sort() for e in sorted_entities]
    return sorted_entities


def __is_clique(entity, relations):
    entity = list(entity)

    for idx, fragment1 in enumerate(entity):
        for idy, fragment2 in enumerate(entity):
            if idx < idy:
                if (fragment1, fragment2) not in [r[0] for r in relations] and (fragment2, fragment1) not in [r[0] for r in relations]:
                    return False

    return True


def __find_subsets(s, subset_len):
    return list(map(set, itertools.combinations(s, subset_len)))


def compute_dependencies(sentence, corenlp):
    edges = corenlp.annotate(sentence).sentence[0].enhancedPlusPlusDependencies.edge
    deps = [set() for t in sentence.split(" ")]
    for edge in edges:
        deps[edge.source-1].add(edge.target-1)
        deps[edge.target-1].add(edge.source-1)
    deps = [list(s) for s in deps]
    [l.sort() for l in deps]
    
    return deps
