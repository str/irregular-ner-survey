import sys
sys.path.append("../src")

import re

import pandas as pd

from pandas_helpers import lists
from entity_processing.array_utils import align_tokenizations, extract_mention_chunks

## LETTERS-DIGITS TOKENIZER FOR DAI20

from typing import NamedTuple

'''Reference url: https://github.com/allenai/allennlp/blob/master/allennlp/data/tokenizers/token.py
Update date: 2019-Nov-5'''
class Token(NamedTuple):
    text: str = None
    start: int = None  # the character offset of this token into the tokenized sentence.


    @property
    def end(self):
        return self.start + len(self.text)


    def __str__(self):
        return self.text


    def __repr__(self):
        return self.__str__()

'''Reference url: https://github.com/allenai/allennlp/blob/master/allennlp/data/tokenizers/letters_digits_tokenizer.py
Update date: 2019-Nov-25

Retrieved from: https://github.com/daixiangau/acl2020-transition-discontinuous-ner/blob/b59d710d975b2bc84072ce749e6ff3b0d0f66815/data/cadec/tokenization.py#L26
'''
class LettersDigitsTokenizer:
    def tokenize(self, text):
        tokens = [Token(m.group(), start=m.start()) for m in re.finditer(r"[^\W\d_]+|\d+|\S", text)]
        return tokens
    

def align_with_dai_tokens(words, comprises):
    tokenizer = LettersDigitsTokenizer()
    
    # Get indices and text of words in the original tokenization
    alignment = words \
        .sort_values(["doc_id", "word_id"]) \
        .groupby(["doc_id", "sentence_id"])[["word_id", "word_text"]].apply(lists)

    # Generate indices and text of tokens extracted using the tokenizer from Dai et al. (2020)
    alignment["sentence"] = alignment["word_text_list"].apply(" ".join)
    alignment["dai_text_list"] = alignment["sentence"].apply(lambda s: [t for t, _ in tokenizer.tokenize(s)])
    alignment["dai_id_list"] = alignment["dai_text_list"].apply(len).apply(range).apply(list)

    # Map each original token to a token according to Dai et al. (2020)
    def align_with_dai(x):
        return pd.Series(align_tokenizations(x["word_id_list"],
                                             x["word_text_list"],
                                             x["dai_id_list"],
                                             x["dai_text_list"]
                                            ),
                         index=["word_id", "dai_id"]
                        )
    aligned_words = alignment.apply(align_with_dai, axis=1).explode(["word_id", "dai_id"]).explode("word_id").explode("dai_id")

    # Bring entity IDs
    aligned_comprises = comprises \
                            .merge(aligned_words.reset_index(), on=["doc_id", "word_id"]) \
                            .sort_values(["doc_id", "word_id"])
    
    return aligned_comprises, alignment


def convert_to_dai_format(aligned_comprises, alignment):
    # The Dai model accepts the original, individual entities
    dai_format = pd.DataFrame()
    dai_format["sentence"] = alignment["sentence"]
    dai_format["boundaries"] = aligned_comprises \
        .groupby(["doc_id", "sentence_id", "entity_id"]) \
        ["dai_id"].apply(list) \
        .apply(extract_mention_chunks) \
        .apply(lambda chunks: [(min(c), max(c)) for c in chunks]) \
        .groupby(["doc_id", "sentence_id"]) \
        .apply(list)
    dai_format.reset_index(level=0, drop=True, inplace=True)
    
    return dai_format

