import pandas as pd
import tensorflow as tf
import tensorflow_addons as tfa
from transformers import AutoTokenizer, AutoConfig

from .sequence_tagger import Mask, SequenceTaggingModel

class ModelTrainer:
    PADDING_TAG = "<pad>"
    
    def __init__(self,
                 tags,
                 word2id,
                 initialized_word_embeddings,
                 char2id,
                 hf_model_name="bert-base-cased",
                 max_sequence_len=256,
                 dropout_rate=0.1,
                 lstm_hidden_size=200,
                 batch_size=16,
                 verbose=False,
                 char_max_len=25,
                 char_embedding_size=25,
                 char_lstm_size=25,
                 custom_name = None
                ):
        # Generate a name for the model if not explicitly specified
        self.name = hf_model_name + "-bilstm-crf" if custom_name is None else custom_name
        
        # Load BERT tokenizer and model config
        self.__max_sequence_len = max_sequence_len
        self.__tokenizer = AutoTokenizer.from_pretrained(hf_model_name, add_prefix_space=True)
        self.__hf_config = AutoConfig.from_pretrained(hf_model_name)
        self.__initializer_range = self.__hf_config.to_dict().get("initializer_range", -1)
        
        # Conversion between categorical labels and integer labels for model
        self.__tag2label = {tag: i for i, tag in enumerate(tags + [self.PADDING_TAG])}
        self.__label2tag = {i: tag for tag, i in self.__tag2label.items()}
        
        # Load word embedding info
        self.__word2id = word2id
        self.__initialized_word_embeddings = initialized_word_embeddings
        
        # Load character embedding info
        self.__char2id = char2id
        self.__char_max_len = char_max_len
        self.__char_embedding_size = char_embedding_size
        self.__char_lstm_size = char_lstm_size
        
        # Load hyperparameters
        self.__dropout_rate = dropout_rate
        self.__lstm_hidden_size = lstm_hidden_size
        self.__batch_size = batch_size
        
        self.verbose = verbose

        self.custom_objects = {
             "Mask": Mask,
             "SequenceTaggingModel": SequenceTaggingModel
        }
    
    def preprocess_data(self,
                        train_doc_ids, train_word_ids, train_texts, train_tags,
                        val_doc_ids, val_word_ids, val_texts, val_tags,
                        test_doc_ids, test_word_ids, test_texts, test_tags,
                       ):
        print(".. Preprocessing train data...")
        train_data = self.__load_data(train_doc_ids, train_word_ids, train_texts, train_tags)
        print(".. Preprocessing val data...")
        val_data = self.__load_data(val_doc_ids, val_word_ids, val_texts, val_tags)
        print(".. Preprocessing test data...")
        test_data = self.__load_data(test_doc_ids, test_word_ids, test_texts, test_tags)
        
        return {
            "train": train_data,
            "val": val_data,
            "test": test_data
        }
            
    def __load_data(self, doc_indices, word_indices, texts, tags):
        # ENCODE TEXTUAL INPUT
        # Map words to word vector IDs
        word_ids = [[self.__word2id[w.lower()] for w in doc] for doc in texts]
        
        # Map word characters to character embedding IDs
        char_ids = [[[self.__char2id[c.lower()] for c in w] for w in doc] for doc in texts]
        
        # Convert string tags to integer labels
        labels = [[self.__tag2label[tag] for tag in doc] for doc in tags]
        
        # Tokenize words according to BERT's tokenization scheme
        print(".... Tokenizing input...")
        encodings = self.__tokenizer(texts, 
                                     is_split_into_words=True,
                                     max_length=self.__max_sequence_len,
                                     padding="max_length", truncation=True
                                    )
        
        # ADJUST ENCODED INPUT TO BERT TOKENIZATION
        print(".... Aligning tokenizations...")
        adjusted_word_ids = []
        adjusted_char_ids = []
        adjusted_labels = []
        mask = []
        for doc_idx, doc_labels in enumerate(labels):
            doc_adj_word_ids = []
            doc_adj_char_ids = []
            doc_adj_labels = []
            doc_mask = []
            
            # Load mappings from BERT tokens to the original word IDs
            token_mappings = encodings.word_ids(batch_index=doc_idx)
            prev_word_idx = None
            for word_idx in token_mappings:
                if word_idx is not None and word_idx != prev_word_idx:
                    doc_adj_word_ids.append(word_ids[doc_idx][word_idx])
                    
                    word_char_ids = [c for c in char_ids[doc_idx][word_idx]][:self.__char_max_len]
                    padding = [0] * max(0, self.__char_max_len - len(char_ids[doc_idx][word_idx]))
                    doc_adj_char_ids.append(word_char_ids + padding)
                    
                    doc_adj_labels.append(doc_labels[word_idx])
                    doc_mask.append(True)
                else:
                    doc_adj_word_ids.append(0)
                    doc_adj_char_ids.append([0] * self.__char_max_len)
                    doc_adj_labels.append(self.__tag2label[self.PADDING_TAG])
                    doc_mask.append(False)
                prev_word_idx = word_idx
 
            adjusted_word_ids.append(doc_adj_word_ids)
            adjusted_char_ids.append(doc_adj_char_ids)
            adjusted_labels.append(doc_adj_labels)
            mask.append(doc_mask)
    
        # Gather useful data
        print(".... Gathering preprocessed data...")
        data = {
            "doc_indices": doc_indices,
            "word_indices": word_indices,
            "input_ids": tf.convert_to_tensor(encodings["input_ids"]),
            "bert_mask": tf.convert_to_tensor(encodings["attention_mask"]),
            "word_ids": tf.convert_to_tensor(adjusted_word_ids),
            "char_ids": tf.convert_to_tensor(adjusted_char_ids),
            "mask": tf.convert_to_tensor(mask),
            "labels": tf.convert_to_tensor(adjusted_labels)
        }

        return data
    
    def fit(self, train_data, val_data, checkpoint_save_path, model_save_path,
            learning_rate=1e-3, bert_learning_rate=5e-5, 
            max_epochs=200, patience=5):
        model = self.__generate_model(bert_learning_rate=bert_learning_rate,
                                      learning_rate=learning_rate)
        
        callbacks = [
            tf.keras.callbacks.EarlyStopping(
                 monitor='val_loss', 
                 mode='min', 
                 verbose=self.verbose, 
                 patience=patience
             ),
             tf.keras.callbacks.ModelCheckpoint(
                 checkpoint_save_path + "/",
                 monitor='val_loss',
                 verbose=self.verbose,
                 save_best_only=True,
                 save_weights_only=True
             )
         ]
        
        with tf.keras.utils.custom_object_scope(self.custom_objects):
            history = model.fit(x=[train_data["input_ids"], 
                                   train_data["bert_mask"], 
                                   train_data["word_ids"], 
                                   train_data["char_ids"], 
                                   train_data["mask"]
                                  ],
                                y=train_data["labels"],
                                validation_data=([val_data["input_ids"], 
                                                  val_data["bert_mask"], 
                                                  val_data["word_ids"], 
                                                  val_data["char_ids"], 
                                                  val_data["mask"]],
                                                  val_data["labels"]
                                                 ),
                                batch_size=self.__batch_size,
                                epochs=max_epochs,
                                callbacks=callbacks,
                                verbose=self.verbose
                                )
            
            # Restore model to the state with best validation loss
            #model = tf.keras.models.load_model(checkpoint_save_path)
            load_status = model.load_weights(checkpoint_save_path + "/")
            load_status.assert_consumed()
        
        model.save(model_save_path, include_optimizer=False)
        
        return model, history
        
    def predict(self, data, model_load_path):
        with tf.keras.utils.custom_object_scope(self.custom_objects):
            model = tf.keras.models.load_model(model_load_path,
                                               compile=False
                                              )
        
        # Infer class probabilities for test data
        labels = model.predict([data["input_ids"], 
                                data["bert_mask"], 
                                data["word_ids"], 
                                data["char_ids"],
                                data["mask"]
                               ],
                               batch_size=self.__batch_size
                              )
        
        # Remove special tokens and padding
        clean_labels = tf.ragged.boolean_mask(labels, data["mask"])
        
        # Decode labels to tags
        tags = [[self.__label2tag[i] for i in l] for l in clean_labels.numpy().tolist()]
        
        # Convert to pandas Series
        predictions = pd.Series(tags, index=data["doc_indices"]).rename("tag").to_frame()
        predictions["word_id"] = data["word_indices"]
        
        # Add "O" tags at the end of truncated sentences
        predictions["len_diff"] = predictions["word_id"].apply(len) - predictions["tag"].apply(len)
        predictions["tag"] = predictions.apply(lambda x: x.tag + ["O"] * x.len_diff, axis=1)
        predictions.drop(columns=["len_diff"], inplace=True)
        
        assert all(predictions["word_id"].apply(len) - predictions["tag"].apply(len) == 0)
        
        return predictions.explode(["word_id", "tag"])
    
    def __generate_model(self,
                         learning_rate,
                         bert_learning_rate,
                         adam_epsilon=1e-8
                        ):
        # Initialize model
        model = SequenceTaggingModel(num_tags=len(self.__tag2label),
                                     bert_config=self.__hf_config,
                                     initialized_word_embeddings=self.__initialized_word_embeddings,
                                     char_vocab_len=len(self.__char2id))
        metrics = [tf.keras.metrics.Accuracy()]

        # Create optimizer
        bert_layers, other_layers = model.get_optimizer_groups()
        bert_optimizer = tf.keras.optimizers.Adam(learning_rate=bert_learning_rate,
                                                  epsilon=adam_epsilon
                                                  )
        regular_optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate,
                                                     epsilon=adam_epsilon
                                                    )
        
        optimizers_and_layers = [(bert_optimizer, bert_layers), (regular_optimizer, other_layers)]
        multi_optimizer = tfa.optimizers.MultiOptimizer(optimizers_and_layers)
        
        # Compile the model
        model.compile(optimizer=multi_optimizer, metrics=metrics)
        
        return model
