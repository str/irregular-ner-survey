import argparse

from dataset_loaders.cadec_loader import CADECLoader
from dataset_loaders.psytar_loader import PsyTARLoader
from entity_processing.representation_depth import generate_depth_representations
from setups import Dataset

LOADERS = {
    "cadec": CADECLoader,
    "psytar": PsyTARLoader
}

def load_dataset(dataset_name, raw_data_dir):    
    loader = LOADERS[dataset_name.lower()](raw_data_dir)
    print("Loading documents...")
    words, entities, comprises = loader.load()
    
    print("Generating depth representations...")
    composes = generate_depth_representations(comprises)
    
    print("Saving tables...")
    dataset = Dataset(dataset_name)
    dataset.save("words")(words)
    dataset.save("entities")(entities)
    dataset.save("comprises")(comprises)
    dataset.save("composes")(composes)
    
    print("Success!")
    
    return words, entities, comprises, composes
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", type=str)
    parser.add_argument("--raw_data_dir", type=str, default="../data/raw")
    args = parser.parse_args()
    
    load_dataset(args.dataset, args.raw_data_dir)
    