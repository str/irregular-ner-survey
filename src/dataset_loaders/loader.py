from abc import ABC, abstractmethod

from tokenizers.pre_tokenizers import Whitespace

class Loader(ABC):
    def __init__(self):
        self.tokenizer = Whitespace()
    
    @abstractmethod
    def load(self):
        pass
    