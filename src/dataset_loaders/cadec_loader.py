import os
import glob

import pandas as pd
from tqdm import tqdm

from .loader import Loader

DATASET_NAME = "cadec"
TEXT_FILE_DIR = "text"  # The actual post text is stored here
ADR_ANNOTATION_DIR = "original"  # The annotations are stored here (incl. the ADR ones)
SCT_ANNOTATION_DIR = "sct"  # MEDDRA concept IDs

class CADECLoader(Loader):
    def __init__(self, raw_data_dir):
        super().__init__()
        self.text_file_dir = os.path.join(raw_data_dir, DATASET_NAME, TEXT_FILE_DIR)
        self.adr_annotation_dir = os.path.join(raw_data_dir, DATASET_NAME, ADR_ANNOTATION_DIR)
        self.sct_annotation_dir = os.path.join(raw_data_dir, DATASET_NAME, SCT_ANNOTATION_DIR)
    
    def load_document(self, doc_id):
        # Load words
        words_tuples = []

        # Use len of previous sentences to adjust word boundaries to reflect position in the doc
        curr_post_len = 0
        curr_word_count = 0

        with open(os.path.join(self.text_file_dir, "{}.txt".format(doc_id)), mode="r") as f:
            for line_id, line in enumerate(f):
                sentence_id = "{}-{}".format(doc_id, line_id)

                # Split into words
                words = self.tokenizer.pre_tokenize_str(line)
                for word_id, (word, boundary) in enumerate(words):                
                    # Adjust word_id and boundary to reflect position in whole doc
                    # Note: Each line contains a newline character in the end
                    adjusted_word_id = curr_word_count + word_id
                    adjusted_boundary = tuple([curr_post_len + b for b in boundary])
                    words_tuples.append((
                        doc_id, sentence_id, adjusted_word_id, word, adjusted_boundary
                    ))

                curr_post_len += len(line)
                curr_word_count += len(words)

        words = pd.DataFrame.from_records(words_tuples,
                                          columns=["doc_id",
                                                   "sentence_id",
                                                   "word_id",
                                                   "word_text",
                                                   "boundary"])
        
        # Load entities
        entities = pd.read_csv(os.path.join(self.adr_annotation_dir, "{}.ann".format(doc_id)),
                              sep="\t", names=["data"], usecols=[1], comment="#"
                              )
        entities["doc_id"] = doc_id
        entities["type"] = entities.data.apply(lambda x: x.split()[0])
        entities["boundary"] = entities.data.apply(lambda y: [tuple([int(e) for e in x.split()]) for x in y.split(maxsplit=1)[1].split(";")])
        entities.drop(columns=["data"], inplace=True)
        entities = entities.loc[entities.type == "ADR"] \
                    .reset_index(drop=True) \
                    .reset_index().rename(columns={"index": "entity_id"})
        
        # Load SNOMED-CT annotations
        sct_annotations = pd.read_csv(os.path.join(self.sct_annotation_dir, "{}.ann".format(doc_id)),
                                      sep="\t", names=["data"], usecols=[1], comment="#",
                                      encoding_errors="ignore"
                                      ).dropna()
        sct_annotations["sct_cui"] = sct_annotations.data.apply(lambda y: [x.split("|", maxsplit=1)[0].strip().split()[0] for x in y.split("+")] if "CONCEPT_LESS" not in y else [])
        sct_annotations["boundary_str"] = sct_annotations.data \
            .apply(lambda y: [tuple([int(e) for e in x.strip().split() if e.isdigit()][-2:]) for x in y.rsplit("|", maxsplit=1)[-1].replace("CONCEPT_LESS", "").split(";")]) \
            .astype(str)
        sct_annotations.drop(columns=["data"], inplace=True)
    
        # Merge annotations with entities
        entities["boundary_str"] = entities["boundary"].astype(str)
        entities = entities.merge(sct_annotations, on=["boundary_str"], how="left")
        entities["sct_cui"] = entities["sct_cui"].apply(lambda c: c if c == c else [])
        entities.drop(columns=["boundary_str"], inplace=True)
        
        # Associate words with entities
        comprises_tuples = []
        for _, e in entities.explode("boundary").iterrows():
            for _, w in words.iterrows():
                # If word starts before entity ends and ends after entity starts
                if w.boundary[0] < e.boundary[1] and w.boundary[1] > e.boundary[0]:
                    comprises_tuples.append((doc_id, e.entity_id, w.word_id))
        comprises = pd.DataFrame.from_records(comprises_tuples, columns=["doc_id", "entity_id", "word_id"])

        # Remove boundary information
        words.drop(columns=["boundary"], inplace=True)
        entities.drop(columns=["type", "boundary"], inplace=True)

        return words, entities, comprises
    
    def load(self):
        words = pd.DataFrame()
        entities = pd.DataFrame()
        comprises = pd.DataFrame()

        for fn in tqdm(glob.glob("{}.txt".format(os.path.join(self.text_file_dir, "*")))):
            doc_id = fn.replace(self.text_file_dir + "/", "").replace(".txt", "")
            nwords, nentities, ncomprises = self.load_document(doc_id)
            words = pd.concat([words, nwords], axis=0)
            entities = pd.concat([entities, nentities], axis=0)
            comprises = pd.concat([comprises, ncomprises], axis=0)
            
        return words, entities, comprises
    