from os import path
import string

import numpy as np
import pandas as pd
from tqdm import tqdm

DATASET_NAME = "psytar"
MAX_NUM_ADR = 30

from .loader import Loader


def levenshtein_distance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]

HARDCODED_FIXES = {
    "m": "am",
    "emotionally": "emotion",
    "r": "are",
    "mostly": "most",
    "needing": "need",
    "sleeping": "sleep",
    "grinding": "grind",
    "feel": "felt",
    "felt": "feel",
    "frustrated": "frusted",
    "etcdiarrhea": "diarrhea",
    "gain": "gained",
    "reoccurence": "recurrence"
}

def is_similar(s1, s2):
    if s1 == s2:
        return True
    
    if s1 in HARDCODED_FIXES and HARDCODED_FIXES[s1] == s2:
        return True
    
    # Account for cases like "," and "',"
    if (not len(s1) == len(s2) == 1) \
        and (len(s1) < 3) and (len(s2) < 3) \
        and all([c in string.punctuation for c in s1]) \
        and all([c in string.punctuation for c in s2]):
        return True
    
    # Do not attempt to match single characters
    if (len(s1) < 2) or (len(s2) < 2):
        return False
    
    if (len(s1) < 6) or (len(s2) < 6):
        edit_distance_th = 1
    else:
        edit_distance_th = 2
        
    return levenshtein_distance(s1, s2) <= edit_distance_th
    

class PsyTARLoader(Loader):
    def __init__(self, raw_data_dir):
        super().__init__()
        self.data_path = path.join(raw_data_dir, DATASET_NAME, "PsyTAR_dataset.xlsx")
        
    def load(self):
        # Load posts
        print(".. Loading posts...")
        text_sheet = pd.read_excel(self.data_path, sheet_name="Sentence_Labeling")
        words = text_sheet[["drug_id", "sentence_index", "sentences"]].rename(columns={
            "drug_id": "doc_id",
            "sentence_index": "sentence_id",
            "sentences": "text"
        }).replace(" ", np.nan).dropna()
        words["sentence_id"] = words.apply(lambda x: "{}-{}".format(x["doc_id"], int(x["sentence_id"])), axis=1)

        # Tokenize sentences and generate word_id
        print(".. Tokenizing sentences...")
        words["word_text"] = words["text"].apply(lambda x: [t for t, _ in self.tokenizer.pre_tokenize_str(x)])
        words = words.explode("word_text").drop(columns=["text"]).reset_index(drop=True)
        words["word_id"] = words.groupby("doc_id").cumcount()
        words = words[["doc_id", "sentence_id", "word_id", "word_text"]]

        # Load annotations and match to word IDs
        print(".. Matching ADR annotations to word IDs...")
        entities = []
        annotation_sheet = pd.read_excel(self.data_path, sheet_name="ADR_Identified")
        annotations = annotation_sheet.dropna(subset=["id"])
        count_annotations = 0
        count_errors = 0
        for _, x in tqdm(annotations.iterrows(), total=len(annotations)):
            sentence_slice = words[words["sentence_id"] == "{}-{}".format(x["drug_id"], x["sentence_index"])]
            word_ids = sentence_slice["word_id"].tolist()
            word_texts = sentence_slice["word_text"].tolist()

            # Iterate over annotation columns
            for adr_sn in range(MAX_NUM_ADR):
                #includes_similar_match = False
                adr = x["ADR{}".format(adr_sn+1)]

                # Stop iterating when across a missing value
                if adr != adr:
                    break
                count_annotations += 1
                     
                # Tokenize annotation
                adr_words = [w for w, _ in self.tokenizer.pre_tokenize_str(adr)]
                
                # Locate all appearances of the first token in the sentence
                possible_starts = [i for i in range(len(word_texts)) if (word_texts[i].lower() == adr_words[0].lower()) \
                                       or (len(adr_words) > 1 and word_texts[i].lower() == (adr_words[0] + adr_words[1]).lower())]
                if len(possible_starts) == 0:
                    possible_starts = [i for i in range(len(word_texts)) if is_similar(word_texts[i].lower(), adr_words[0].lower())]
                    #if len(possible_starts) > 0:
                        #includes_similar_match = True
                
                matches = []
                for s in possible_starts:
                    # Check if there is a match starting at `s`
                    i = s
                    word_indices = []
                    match_count = 0
                    adr_i = 0
                    while adr_i < len(adr_words):
                        w = adr_words[adr_i]
                        word_matched = False
                        while (i < len(word_texts)) and (not word_matched):
                            if (word_texts[i].lower() == w.lower()):
                                word_indices.append(word_ids[i])
                                match_count += 1
                                word_matched = True
                                i += 1
                            elif is_similar(word_texts[i].lower(), w.lower()):
                                word_indices.append(word_ids[i])
                                match_count += 1
                                word_matched = True
                                i += 1
                                #includes_similar_match = True
                            elif w.lower() in [",", ";", "-"]:
                                #print("Not True but comma")
                                match_count += 1
                                word_matched = True
                            else:
                                if adr_i < len(adr_words) - 1:
                                    next_word = adr_words[adr_i+1]
                                    if (word_texts[i].lower() == (w + next_word).lower()):
                                        word_indices.append(word_ids[i])
                                        match_count += 2
                                        word_matched = True
                                        adr_i += 1
                                i += 1
                        adr_i += 1
                        word_matched = False
                    if match_count == len(adr_words):
                        matches.append(word_indices)
                        
                if len(matches) == 0:
                    count_errors += 1
                    continue
                        
                if len(matches) > 0:
                    match_lengths = [sum([w1-w0 for i, w0 in enumerate(m) for w1 in m[i+1:]]) for m in matches]
                    min_length = min(match_lengths)
                    all_matches = matches
                    matches = [m for i, m in enumerate(matches) if match_lengths[i] == min_length]
    
                for m in matches:
                    entities.append((x["drug_id"], m, x["drug_id"], x["sentence_index"], adr.lower().strip()))
                
        print(".... Processed {} annotations to extract {} entities. Failed to detect {} annotations.".format(
            count_annotations, len(entities), count_errors
        ))
        entities = pd.DataFrame.from_records(entities, columns=["doc_id", "word_id", "drug_id", "sentence_index", "ADRs"])
        
        # Load SNOMED-CT annotations
        print(".. Adding SNOMED-CT annotations...")
        sct_sheet = pd.read_excel(self.data_path, sheet_name="ADR_Mapped")
        sct_sheet["ADRs"] = sct_sheet["ADRs"].str.lower().str.strip()
        sct_sheet["SNOMED-CT"] = sct_sheet["SNOMED-CT"].str.extract(r"((?<=/)[^/]*(?=]))")
        sct_sheet["SNOMED-CT.1"] = sct_sheet["SNOMED-CT.1"].str.extract(r"((?<=/)[^/]*(?=]))")
        sct_sheet["sct_cui"] = sct_sheet.apply(lambda x: [c for c in [x["SNOMED-CT"], x["SNOMED-CT.1"]] if c == c], axis=1)

        # Merge entities with SNOMED-CT annotations
        entities.set_index(["drug_id", "sentence_index", "ADRs"], inplace=True)
        sct_sheet.set_index(["drug_id", "sentence_index", "ADRs"], inplace=True)
        entities = entities.join(sct_sheet["sct_cui"], how="left").reset_index(drop=True) \
            .sort_values("word_id")

        print(".. Wrapping up...")
        
        # Generate entity_id
        entities["entity_id"] = entities.groupby("doc_id").cumcount()

        # Store word-entity mappings in `comprises`
        comprises = entities.explode("word_id")[["doc_id", "word_id", "entity_id"]]

        # Remove word IDs from `entities`
        entities.drop(columns=["word_id"], inplace=True)
        
        return words, entities, comprises
    