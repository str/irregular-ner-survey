import pandas as pd

def lists(x):
    """
    A version of `list` that can receive and return multiple columns.
    It works with a dataframe under a `groupby` condition, as follows:
    
        `df.groupby(group_col)[[col_1, col_2]].apply(lists)`
        
    It additionally appends "_list" to each column name.
    
    Parameters:
    x (pd.Series)    The records collected by the `groupby` function
    
    Returns:
    pd.Series        The list produced for each column
    """
    return pd.Series([list(x[c].dropna()) for c in x.columns], 
                     index=["{}_list".format(c) for c in x.columns])