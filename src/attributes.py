import string

import pandas as pd

from entity_processing.array_utils import is_discontinuous, check_overlap_location

BUCKET_NAMES = ["None", "XS", "S", "L", "XL"]
BUCKET_EDGES = {
    "entity_length": [0, 1, 2, 3, 5, 1000],
    "interval_length": [0, 1, 2, 3, 4, 1000],
    "sentence_length": [-1, 0, 12, 19, 30, 240],
    "entity_density": [-1, 0.018182, 0.285714, 0.515152, 0.666667, 10],
    "entity_token_density": [0, 0.018182, 0.276596, 0.500000, 0.642857, 10],
    "oov_density": [-1, 0, 0.02, 0.25, 0.5, 1.1],
    "entity_frequency": [-1, 0, 0.0001, 0.000790, 0.004625, 1],
    "token_frequency": [-1, 0, 0.000001, 0.000638, 0.002937, 1],
    "token_consistency": [-1, 0, 0.01, 0.655948, 1, 1000]
}


def compute_local_attributes(entities, comprises, words):
    entity_attributes = entities.copy()
    entity_attributes["is_discontinuous"] = compute_discontinuity(entities, comprises)
    entity_attributes["overlap_location"] = compute_overlap_location(entities, comprises)
    entity_attributes["is_overlapping"] = compute_overlapping(entities, comprises)
    entity_attributes["entity_length"] = compute_entity_length(entities, comprises)
    entity_attributes["interval_length"] = compute_interval_length(entities, comprises)
    entity_attributes["sentence_length"] = compute_sentence_length(entities, comprises, words)
    entity_attributes["entity_density"] = compute_entity_density(entities, comprises, words)
    entity_attributes["entity_token_density"] = compute_entity_token_density(entities, comprises, words)
    
    entity_attributes = discretize_attributes(entity_attributes)
    
    return entity_attributes


def compute_aggregate_attributes(entities, comprises, words, train_sentences, train_comprises):
    entity_attributes = entities.copy()
    entity_attributes["oov_density"] = \
        compute_oov_density(entities, comprises, words, 
                            train_sentences)
    entity_attributes["entity_frequency"] = \
        compute_entity_frequency(entities, comprises, words,
                                 train_comprises)
    entity_attributes["token_frequency"] = \
        compute_token_frequency(entities, comprises, words, 
                                train_sentences)
    entity_attributes["token_consistency"] = \
        compute_token_consistency(entities, comprises, words, 
                                  train_sentences, train_comprises)
    
    entity_attributes = discretize_attributes(entity_attributes)
    return entity_attributes


def discretize_attributes(attribute_df):
    new_df = attribute_df.copy()
    for a in attribute_df.columns:
        if a in BUCKET_EDGES:
            new_df["{}_d".format(a)] = pd.cut(attribute_df[a], BUCKET_EDGES[a], 
                                              right=False, labels=BUCKET_NAMES)
    return new_df


def compute_discontinuity(entities, comprises):
    out = entities.join(
        comprises
            .groupby(["doc_id", "sentence_id", "entity_id"]) \
            ["word_id"].apply(list) \
            .apply(is_discontinuous) \
            .rename("is_discontinuous")
        )["is_discontinuous"]
    assert entities.index.equals(out.index)
    return out


def compute_overlapping(entities, comprises):
    word_entities = comprises \
        .groupby(["doc_id", "sentence_id", "word_id"])["entity_id"] \
        .apply(set)
    shared_words = word_entities[word_entities.apply(len) > 1].explode().reset_index() \
                        .groupby(["doc_id", "sentence_id", "entity_id"])["word_id"].apply(set) \
                        .rename("shared_words")
    entity_words = comprises.groupby(["doc_id", "sentence_id", "entity_id"])["word_id"].apply(set) \
                    .rename("all_words")
    out = entities.join(entity_words).join(shared_words, how="left") \
            .apply(lambda x: False if check_overlap_location(x["all_words"], x["shared_words"]) == "none" else True, 
                   axis=1) \
            .rename("is_overlapping")
    assert entities.index.equals(out.index)
    return out    


def compute_overlap_location(entities, comprises):
    word_entities = comprises \
        .groupby(["doc_id", "sentence_id", "word_id"])["entity_id"] \
        .apply(set)
    shared_words = word_entities[word_entities.apply(len) > 1].explode().reset_index() \
                        .groupby(["doc_id", "sentence_id", "entity_id"])["word_id"].apply(set) \
                        .rename("shared_words")
    entity_words = comprises.groupby(["doc_id", "sentence_id", "entity_id"])["word_id"].apply(set) \
                    .rename("all_words")
    out = entities.join(entity_words).join(shared_words, how="left") \
            .apply(lambda x: check_overlap_location(x["all_words"], x["shared_words"]), axis=1) \
            .rename("overlap_location")
    assert entities.index.equals(out.index)
    return out


def compute_entity_length(entities, comprises):
    out = entities.join(
        comprises.groupby(["doc_id", "sentence_id", "entity_id"]) \
            ["word_id"].apply(len).rename("entity_length")
    )["entity_length"]
    assert entities.index.equals(out.index)
    return out


def compute_interval_length(entities, comprises):
    out = entities.join(
        comprises.groupby(["doc_id", "sentence_id", "entity_id"]) \
            ["word_id"].apply(lambda x: 1+x.max() - x.min() - len(x)) \
            .rename("interval_length")
    )["interval_length"]
    assert entities.index.equals(out.index)
    return out


def compute_sentence_length(entities, comprises, words):
    sentence_length = words.groupby(["doc_id", "sentence_id"]) \
        ["word_id"].apply(len).rename("sentence_length")

    out = entities.join(
        comprises \
            .drop_duplicates(["doc_id", "sentence_id", "entity_id"]) \
            .set_index(["doc_id", "sentence_id"])  \
            .join(sentence_length, how="left") \
            .set_index("entity_id", append=True)
    )["sentence_length"]
    assert entities.index.equals(out.index)
    return out


def compute_entity_density(entities, comprises, words):
    sentence_length = words.groupby(["doc_id", "sentence_id"]) \
            ["word_id"].apply(len)
        
    entity_count = comprises.groupby(["doc_id", "sentence_id"])["entity_id"] \
        .apply(len)
    
    entity_density = (entity_count / sentence_length).rename("entity_density")
    
    out = entities.join(
        comprises \
            .drop_duplicates(["doc_id", "sentence_id", "entity_id"]) \
            .set_index(["doc_id", "sentence_id"]) \
            .join(entity_density, how="left") \
            .set_index("entity_id", append=True)
    )["entity_density"]
    assert entities.index.equals(out.index)
    return out


def compute_entity_token_density(entities, comprises, words):
    sentence_length = words.groupby(["doc_id", "sentence_id"]) \
            ["word_id"].apply(len)
    
    entity_token_count = comprises \
        .drop_duplicates(["doc_id", "sentence_id", "word_id"]) \
        .groupby(["doc_id", "sentence_id"])["word_id"] \
        .apply(len)
        
    entity_token_density = (entity_token_count / sentence_length).rename("entity_token_density")
    
    out = entities.join(
        comprises \
            .drop_duplicates(["doc_id", "sentence_id", "entity_id"]) \
            .set_index(["doc_id", "sentence_id"]) \
            .join(entity_token_density, how="left") \
            .set_index("entity_id", append=True)
    )["entity_token_density"]
    assert entities.index.equals(out.index)
    return out  


def compute_oov_density(entities, comprises, words, train_sentences):
    # Find words that are part of the vocabulary in the training and validation sets
    train_vocabulary = words[words["sentence_id"].isin(train_sentences)]["word_text"] \
                        .str.lower().drop_duplicates().tolist()
    
    # Find percentage of words not in the training vocabulary for each entity
    entity_texts = comprises.merge(words)
    entity_texts["oov"] = ~entity_texts["word_text"].str.lower().isin(train_vocabulary)
    
    out = entities.join(
        entity_texts.groupby(["doc_id", "sentence_id", "entity_id"])["oov"].mean()
    )
    assert entities.index.equals(out.index)
    return out


def compute_entity_frequency(entities, comprises, words, train_comprises):
    def compute_entity_texts(comprises):
        return comprises.merge(words).sort_values(["doc_id", "word_id"]) \
                    .assign(word_text=lambda df: df["word_text"].str.lower()) \
                    .drop_duplicates(["doc_id", "sentence_id", "entity_id", "word_id"]) \
                    .groupby(["doc_id", "sentence_id", "entity_id"]) \
                    ["word_text"].apply(list).apply(" ".join) \
                    .to_frame()
    train_entity_texts = compute_entity_texts(train_comprises)
    test_entity_texts = compute_entity_texts(comprises)

    entity_freqs = train_entity_texts.groupby("word_text") \
                    .apply(len).rename("entity_frequency")
    rel_entity_freqs = entity_freqs / entity_freqs.sum()
    
    out = entities.join(
        test_entity_texts.reset_index() \
            .merge(rel_entity_freqs, on="word_text", how="left") \
            .fillna(0) \
            .set_index(["doc_id", "sentence_id", "entity_id"]) \
            ["entity_frequency"]
    )
    assert entities.index.equals(out.index)
    return out


def compute_token_frequency(entities, comprises, words, train_sentences):
    # Compute frequency of tokens throughout the whole (training) data
    token_freqs = words[words["sentence_id"].isin(train_sentences)]\
                    .assign(word_text=lambda df: df["word_text"].str.lower()) \
                    .groupby("word_text").apply(len).rename("token_frequency")
    
    # Compute relative frequency of tokens over the training set EXCLUDING basic punctuation
    token_rel_freqs = token_freqs / token_freqs.sum()
    entity_token_frequency = \
        words.merge(comprises).assign(word_text=lambda df: df["word_text"].str.lower()) \
        .merge(token_rel_freqs[~token_rel_freqs.index.isin(list(string.punctuation))].reset_index(),
               on=["word_text"], how="left") \
        .groupby(["doc_id", "sentence_id", "entity_id"])["token_frequency"] \
        .mean() \
        .fillna(0)
    
    out = entities.join(entity_token_frequency)["token_frequency"]
    assert entities.index.equals(out.index)
    return out


def compute_token_consistency(entities, comprises, words, train_sentences, train_comprises):
    # Compute frequency of tokens throughout the whole (training) data
    token_freqs = words[words["sentence_id"].isin(train_sentences)] \
                    .assign(word_text=lambda df: df["word_text"].str.lower()) \
                    .groupby("word_text").apply(len).rename("token_frequency")
    
    # Compute frequency of tokens in entities
    token_entity_freqs = words[words["sentence_id"].isin(train_sentences)].merge(train_comprises) \
        .drop_duplicates(["doc_id", "word_id"]) \
        .assign(word_text=lambda df: df["word_text"].str.lower()) \
        .groupby("word_text").apply(len)
    token_label_consistency = ( token_entity_freqs / token_freqs ).rename("token_consistency").fillna(1)
    
    entity_token_label_consistency = words.merge(comprises) \
        .assign(word_text=lambda df: df["word_text"].str.lower()) \
        .merge(token_label_consistency[~token_label_consistency.index.isin(list(string.punctuation))].reset_index(), 
               on=["word_text"], how="left") \
        .groupby(["doc_id", "sentence_id", "entity_id"])["token_consistency"] \
        .mean() \
        .fillna(0)

        
    out = entities.join(entity_token_label_consistency)["token_consistency"]
    assert entities.index.equals(out.index)
    return out
        