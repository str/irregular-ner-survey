import pandas as pd

NUM_CONCEPTS = 5

def make_dictionary(concepts, 
                    cui_column="sct_cui",
                    synonym_column="synonyms"
                   ):
    return (concepts[cui_column].astype(str) + "||" + \
            concepts[synonym_column].str.join("|")) \
            .tolist()

def make_queries(words, comprises, entities, with_ground_truth=False):
    # Compile sentences (contexts)
    contexts = words.groupby("sentence_id")["word_text"].apply(list) \
                .str.join(" ").rename("sentence_text")
    
    # Compile mentions
    entities_with_concept = entities.copy()
    if with_ground_truth:
        # Concepts can be composite, as per preprocessing by BioSyn
        entities_with_concept["sct_cui"] = entities_with_concept["sct_cui"] \
            .apply(lambda cs: "|".join(cs) if len(cs) > 0 else "-1")
    else:
        entities_with_concept["sct_cui"] = "-1"
        
    mentions = entities_with_concept.merge(comprises).merge(words) \
        .groupby(["doc_id", "sentence_id", "entity_id", "sct_cui"])["word_text"] \
        .apply(list).str.join(" ").reset_index()
    mentions["mention_line"] = mentions["sentence_id"] \
            + "||" + "_" \
            + "||" + "_" \
            + "||" + mentions["word_text"] \
            + "||" + mentions["sct_cui"]
    
    # Full BioSyn input
    return contexts.to_frame() \
        .join(mentions.groupby("sentence_id")["mention_line"].apply(list))

def make_inference_queries(words, comprises):
    # Compile mentions
    mentions = comprises.merge(words) \
                .groupby(["doc_id", "sentence_id", "entity_id"])["word_text"] \
                .apply(list).str.join(" ")
    return mentions.index.values.tolist(), mentions.values.tolist()

def parse_predictions(ids, pdict):
    mentions = []
    for mention in pdict["res"]:
        top_concepts = []
        candidates = mention["predictions"]
        for c in candidates:
            for cui in c["id"].split("|"):
                #print(cui, top_concepts)
                if (int(cui) not in top_concepts) and (len(top_concepts) <= NUM_CONCEPTS):
                    top_concepts.append(int(cui))
                
            if len(top_concepts) == NUM_CONCEPTS:
                break
    
        mentions.append(top_concepts)
    return pd.Series(mentions, 
                     index=pd.MultiIndex.from_tuples(ids, 
                                                     names=["doc_id", 
                                                            "sentence_id", 
                                                            "entity_id"]),
                     name="cui_id")
    