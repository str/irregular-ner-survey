import sys
sys.path.append("../src")

import argparse
from os import path, makedirs

import pandas as pd
from tqdm import tqdm

from setups import SCT_SAVE_PATH

DEFAULT_SCT_FOLDER = "../tmp/SnomedCT_InternationalRF2_PRODUCTION_20210731T120000Z"
CLINICAL_FINDING_CUI = 404684003
ISA_RELATIONSHIP_TYPEID = 116680003
FSN_TYPEID = 900000000000003001
SYNONYM_TYPEID = 900000000000013009

SCT_SAVE_PATH = "../data/sct/concepts_synonyms.pkl"


def load_sct(sct_folder=DEFAULT_SCT_FOLDER, reload_from_disk=False, save=False):
    if reload_from_disk:
        data_path = path.join(sct_folder, "Snapshot/Terminology")

        # Load relationships
        print("Loading relationships...")
        relations = pd.read_csv(path.join(data_path, "sct2_Relationship_Snapshot_INT_20210731.txt"),
                                sep="\t")

        # Filter IS-A relationships (child is parent)
        isa_relations = relations[relations["typeId"] == ISA_RELATIONSHIP_TYPEID]

        # Traverse relations to retrieve all CUIs under "Clinical finding"
        print("Retrieving relevant concepts...")
        relevant_concepts = set()
        leaf_level = set()
        stack = [CLINICAL_FINDING_CUI]
        with tqdm(total=155694) as pbar:
            while len(stack) > 0:
                checked_cui = stack.pop()
                # Only check each concept once
                if checked_cui in relevant_concepts:
                    continue
                relevant_concepts.add(checked_cui)
                pbar.update()
                child_cuis = isa_relations[isa_relations["destinationId"] == checked_cui] \
                                ["sourceId"].values
                stack.extend(child_cuis)
                if len(child_cuis) == 0:
                    leaf_level.add(checked_cui)

        # Load descriptions
        print("Loading descriptions...")
        descriptions = pd.read_csv(path.join(data_path, "sct2_Description_Snapshot-en_INT_20210731.txt"),
                                   sep="\t")
        descriptions["sct_cui"] = descriptions["conceptId"]

        # Find FSN and Synonyms for the relevant concepts
        print("Generating dataframe...")
        relevant_descriptions = \
            descriptions[descriptions["sct_cui"].isin(relevant_concepts) & 
                         descriptions["active"] == 1]
        fsn = relevant_descriptions[relevant_descriptions["typeId"] == FSN_TYPEID] \
                .set_index("sct_cui")["term"].rename("fsn").to_frame()
        assert fsn.index.is_unique

        synonyms = relevant_descriptions[relevant_descriptions["typeId"] == SYNONYM_TYPEID] \
                    .groupby("sct_cui")["term"].apply(list).rename("synonyms")

        concepts = fsn.join(synonyms, how="left")
        concepts.loc[concepts["synonyms"] != concepts["synonyms"], "synonyms"] = \
            concepts["fsn"].apply(lambda x: [x])

        # Store data
        if save:
            print("Saving...")
            if not path.exists(path.dirname(SCT_SAVE_PATH)):
                makedirs(path.dirname(SCT_SAVE_PATH))
            concepts.to_pickle(SCT_SAVE_PATH)

        print("Success!")
        
    else:
        concepts = pd.read_pickle(SCT_SAVE_PATH)
    
    return concepts
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--sct_folder", type=str, default=DEFAULT_SCT_FOLDER)
    args = parser.parse_args()
    
    load_sct(args.sct_folder, reload_from_disk=True, save=True)
    