from os import path, makedirs
import json
from ast import literal_eval as make_tuple
import re
import pickle

from tqdm import tqdm
import numpy as np
import pandas as pd
from sklearn.utils import resample
from scipy.stats import pointbiserialr

from linking.load_sct import load_sct
from linking.preprocess import make_dictionary, make_queries, \
                                make_inference_queries, parse_predictions
from models.dai20 import align_with_dai_tokens, convert_to_dai_format
from models.li21 import generate_li_input_entries, convert_chunks_relations_to_boundaries
from entity_processing.array_utils import split_composite_entity
from entity_processing.representation_depth import generate_depth_representations
from evaluation.matches import compute_matches
from tagging_formats.reconstruction import BIOHD, BIOHD1234, FuzzyBIO, BIOO
from pandas_helpers import lists
from attributes import compute_local_attributes, compute_aggregate_attributes


BIOSYN_DATA_FOLDER = "../data/external/biosyn"


def store_biosyn_queries(queries, save_dir):    
    if not path.exists(save_dir):
        makedirs(save_dir)
        
    for index, s in tqdm(queries.iterrows(), total=len(queries)):
        # Store context
        context_fp = path.join(save_dir, "{}.txt".format(index))
        with open(context_fp, "w") as f:
            f.write(s["sentence_text"] + "\n")
    
        # Store mentions
        mention_fp = path.join(save_dir, "{}.concept".format(index))
        with open(mention_fp, "w") as f:
            if s["mention_line"] == s["mention_line"]:
                f.writelines(["{}\n".format(l) for l in s["mention_line"]])
            else:
                f.write("\n")

                
def preprocess_entities_for_biosyn(words, comprises, save_dir):
    ids, mentions = make_inference_queries(words, comprises)

    if not path.exists(save_dir):
        makedirs(save_dir)

    # Store IDs
    with open(path.join(save_dir, "mentions.id"), "w") as f:
        f.writelines(["{}\n".format(i) for i in ids])

    # Store predictions
    with open(path.join(save_dir, "mentions.txt"), "w") as f:
        f.writelines(["{}\n".format(m) for m in mentions])

        
def process_biosyn_predictions(ids_dir, predictions_dir):
    with open(path.join(ids_dir, "mentions.id"), mode="r") as f:
        ids = [make_tuple(l.replace("\n", "")) for l in f.readlines()]
        
    with open(path.join(predictions_dir, "predictions.json"), mode="r") as f:
        pdict = json.load(f)
        
    return parse_predictions(ids, pdict)

        
class Pipeline:
    def __init__(self, setup):
        self.setup = setup

        
class DatasetPipeline(Pipeline):
    def __init__(self, setup):
        super().__init__(setup)

    def compute_attributes(self):
        words = self.setup.load("words")
        comprises = self.setup.load("comprises") \
                        .merge(words)
        entities = comprises[["doc_id", "sentence_id", "entity_id"]] \
                        .drop_duplicates() \
                        .set_index(["doc_id", "sentence_id", "entity_id"])
        
        print("Computing attributes...")
        entity_attributes = compute_local_attributes(entities, comprises, words)
        
        # Fix overlap location for PsyTAR duplicates
        duplicates = comprises.groupby(["doc_id", "sentence_id", "entity_id"])["word_id"] \
                        .apply(list).astype(str).apply("".join) \
                        .reset_index().set_index(["doc_id", "sentence_id", "entity_id"], drop=False) \
                        .duplicated(["doc_id", "sentence_id", "word_id"], keep=False)
        entity_attributes.loc[duplicates, "overlap_location"] = "duplicated"
        
        print("Storing to disk...")
        self.setup.save("entity_attributes")(entity_attributes, depth="individual")
        
        print("Success!")
        
        return entity_attributes
    
        
class SamplePipeline(Pipeline):
    def __init__(self, setup):
        super().__init__(setup)
        self.biosyn_train_queries = None
        
    def compute_fold_attributes(self, folds_to_use=range(0, 10, 2), 
                                unit_of_analysis="sentence"):
        print("Computing fold attributes...")
        words = self.setup.load("words")
        comprises = self.setup.load("comprises") \
                        .merge(words)
        entities = comprises[["doc_id", "sentence_id", "entity_id"]] \
                        .drop_duplicates() \
                        .set_index(["doc_id", "sentence_id", "entity_id"])
        folds = self.setup.load("folds", unit_of_analysis=unit_of_analysis)
        
        for fold in folds_to_use:
            print("FOLD", fold, "                 ")
            train_sentences = folds[fold]["train"] + folds[fold]["val"]
            train_comprises = comprises[comprises["sentence_id"].isin(train_sentences)]
            test_entities = entities[entities.index.get_level_values("sentence_id")
                                     .isin(folds[fold]["test"])]
            
            print("Computing...", end="\r")
            entity_attributes = \
                compute_aggregate_attributes(test_entities, comprises, words,
                                             train_sentences, train_comprises)
            
            print("Storing to disk...       ", end="\r")
            self.setup.save("fold_entity_attributes")(entity_attributes, 
                                                      depth="individual", 
                                                      fold=fold)
        print("Success!           ")
            

    ## BIOSYN ##
    def prepare_biosyn_training(self, unit_of_analysis, folds_to_use=range(0, 10, 2)):
        # Prepare dictionary
        print("Preparing dictionary...")
        self.__build_biosyn_dictionary()
        
        # Prepare training set
        print("Preparing train sets...")
        for fold in folds_to_use:
            self.__build_train_set(fold, unit_of_analysis)
            
        # Prepare test set
        print("Preparing test sets...")
        for fold in folds_to_use:
            self.__build_eval_set(fold, unit_of_analysis)
            
        # Prepare folder for evaluation results
        print("Creating BioSyn output folders...")
        for fold in folds_to_use:
            output_dir = path.join(BIOSYN_DATA_FOLDER, 
                                   "output", 
                                   self.setup.dataset.name, 
                                   "gold_eval", str(fold))
            if not path.exists(output_dir):
                makedirs(output_dir)
                
        print("Done! Navigate to the BioSyn root folder and train with the following command:",
              "\nbash train.sh {} [fold] [cuda_device]".format(self.setup.dataset.name),
              "\n\n or evaluate with:",
              "\nbash eval.sh {} [fold] [cuda_device] gold_eval".format(self.setup.dataset.name),
             )
    
    def preprocess_annotations_for_biosyn(self, fold, unit_of_analysis):
        words = self.setup.load("words")
        comprises = self.setup.load("comprises")
        
        # Filter words to only include test sentences
        folds = self.setup.load("folds", unit_of_analysis=unit_of_analysis)
        words = words[words["sentence_id"].isin(folds[fold]["test"])]
        
        subfolders = (self.setup.dataset.name, "gold_test", str(fold))
        preprocess_entities_for_biosyn(words, comprises,
                                       path.join(BIOSYN_DATA_FOLDER,
                                                 "input",
                                                 *subfolders
                                                ))
        
        output_dir = path.join(BIOSYN_DATA_FOLDER, "output", *subfolders)
        if not path.exists(output_dir):
            makedirs(output_dir)
            
    def load_biosyn_concepts_for_annotations(self, fold):
        subfolders = (self.setup.dataset.name, "gold_test", str(fold))
        ids_dir = path.join(BIOSYN_DATA_FOLDER, "input", *subfolders)
        pdict_dir = path.join(BIOSYN_DATA_FOLDER, "output", *subfolders)
        
        formatted_concepts = process_biosyn_predictions(ids_dir, pdict_dir)
        
        self.setup.save("predicted_ann_concepts")(formatted_concepts, fold=fold)
        
        return formatted_concepts
    
    def __build_biosyn_dictionary(self):
        concepts = load_sct()
        dictionary = make_dictionary(concepts.reset_index())
        
        dictionary_path = path.join(BIOSYN_DATA_FOLDER, "train", "dictionary.txt")
        
        if not path.exists(path.dirname(dictionary_path)):
            makedirs(path.dirname(dictionary_path))

        with open(dictionary_path, "w") as f:
            f.writelines(["{}\n".format(t) for t in dictionary])
    
    def __build_train_set(self, fold, unit_of_analysis):      
        train_queries = self.__build_set("train", fold, unit_of_analysis)
        
        # Store to disk
        store_biosyn_queries(train_queries.dropna(),
                             path.join(BIOSYN_DATA_FOLDER,
                                       "train",
                                       self.setup.dataset.name, 
                                       str(fold)))
        
    def __build_eval_set(self, fold, unit_of_analysis):
        eval_queries = self.__build_set("eval", fold, unit_of_analysis)
        
        # Store to disk
        store_biosyn_queries(eval_queries.dropna(),
                             path.join(BIOSYN_DATA_FOLDER,
                                       "input",
                                       self.setup.dataset.name,
                                       "gold_eval",
                                       str(fold)))
        
    def __build_set(self, mode, fold, unit_of_analysis, force_preprocessing=False):
        # Ensure that entities have been brought to BioSyn format
        if (self.biosyn_train_queries is None) or force_preprocessing:
            self.__preprocess_entities_for_biosyn_training()
            
        # Filter entities in train and validation set
        f = self.setup.load("folds", unit_of_analysis=unit_of_analysis)
        q = self.biosyn_train_queries
        
        if mode == "train":
            return q[q.index.isin(f[fold]["train"] + f[fold]["val"])]
        elif mode == "eval":
            return q[q.index.isin(f[fold]["test"])]
        
    def __preprocess_entities_for_biosyn_training(self):
        words = self.setup.load("words")
        comprises = self.setup.load("comprises")
        entities = self.setup.load("entities")

        self.biosyn_train_queries = make_queries(words, comprises, entities,
                                                 with_ground_truth=True
                                                )
        
        
class ModelPipeline(Pipeline):
    def __init__(self, setup):
        super().__init__(setup)

    def compute_attributes(self,
                           folds_to_use=range(0, 10, 2),
                           unit_of_analysis="sentence"):
        words = self.setup.load("words")
        folds = self.setup.load("folds", unit_of_analysis=unit_of_analysis)
        full_train_comprises = self.setup.load("comprises") \
                                .merge(words)
        for fold in folds_to_use:
            print("FOLD", fold, "                ")
            comprises = self.setup.load("predicted_comprises", 
                                        unit_of_analysis=unit_of_analysis, 
                                        fold=fold) \
                            .merge(words)
            entities = comprises[["doc_id", "sentence_id", "entity_id"]] \
                            .drop_duplicates() \
                            .set_index(["doc_id", "sentence_id", "entity_id"])
            train_sentences = folds[fold]["train"] + folds[fold]["val"]
            train_comprises = full_train_comprises \
                [full_train_comprises["sentence_id"].isin(train_sentences)]
            
            print("Computing attributes...", end="\r")
            local_attributes = compute_local_attributes(entities, comprises, words)
            aggregate_attributes = compute_aggregate_attributes(entities, comprises,
                                                                words, train_sentences,
                                                                train_comprises)
            entity_attributes = pd.concat([local_attributes, aggregate_attributes],
                                          axis=1)
        
            print("Storing to disk...       ", end="\r")
            self.setup.save("predicted_entity_attributes")(entity_attributes,
                                                           unit_of_analysis=unit_of_analysis,
                                                           fold=fold,
                                                           depth="individual")
        
        print("Success!               ")
        
    def preprocess_predictions_for_biosyn(self, 
                                          folds_to_use=range(0, 10, 2), 
                                          unit_of_analysis="sentence"):
        print("Preprocessing predicted spans for BioSyn...")
        
        words = self.setup.load("words")
        for fold in folds_to_use:
            comprises = self.setup.load("predicted_comprises", 
                                        fold=fold, unit_of_analysis=unit_of_analysis)

            subfolders = (self.setup.sample.dataset.name, 
                          self.setup.get_full_model_name(), 
                          str(fold)
                         )
            preprocess_entities_for_biosyn(words, comprises,
                                           path.join(BIOSYN_DATA_FOLDER,
                                                     "input",
                                                     *subfolders))

            output_dir = path.join(BIOSYN_DATA_FOLDER, "output", *subfolders)
            if not path.exists(output_dir):
                makedirs(output_dir)
 
    def load_biosyn_concepts_for_predictions(self, 
                                             folds_to_use=range(0, 10, 2),
                                             unit_of_analysis="sentence"
                                            ):
        print("Parsing BioSyn predictions...")
        
        res = {}
        for fold in folds_to_use:
            subfolders = (self.setup.sample.dataset.name, 
                          self.setup.get_full_model_name(), 
                          str(fold)
                         )
            ids_dir = path.join(BIOSYN_DATA_FOLDER, "input", *subfolders)
            pdict_dir = path.join(BIOSYN_DATA_FOLDER, "output", *subfolders)

            formatted_concepts = process_biosyn_predictions(ids_dir, pdict_dir)

            self.setup.save("predicted_pred_concepts")(formatted_concepts,
                                                       unit_of_analysis=unit_of_analysis,
                                                       fold=fold)
            res[fold] = formatted_concepts
        
        return res

    def compute_boundary_matches(self,
                                 folds_to_use=range(0, 10, 2),
                                 unit_of_analysis="sentence", 
                                 depth="individual"):
        print("Computing boundary matches...")
        
        # Compute matches
        print(".. Computing matches...")
        matches = self.compute_matches("boundary", folds_to_use, unit_of_analysis, depth)
        
        # Storing to disk
        print(".. Storing to disk...")
        for fold in folds_to_use:
            self.setup.save("matches")(matches[fold],
                                       fold=fold,
                                       depth=depth,
                                       unit_of_analysis=unit_of_analysis
                                      )
        
        print(".. Success!")
        
        return matches

    def compute_concept_matches(self,
                                folds_to_use=range(0, 10, 2),
                                unit_of_analysis="sentence", 
                                depth="individual"):
        print("Computing concept matches...")
        
        # Compute matches
        print(".. Computing matches...")
        matches = self.compute_matches("concept", folds_to_use, unit_of_analysis, depth)
        
        # Storing to disk
        print(".. Storing to disk...")
        for fold in folds_to_use:
            self.setup.save("concept_matches")(matches[fold],
                                               fold=fold,
                                               depth=depth,
                                               unit_of_analysis=unit_of_analysis
                                              )
        
        print(".. Success!")
        
        return matches    
    
    def compute_matches(self, match_type, folds_to_use, unit_of_analysis, depth):
        folds = self.setup.load("folds", unit_of_analysis=unit_of_analysis)
        
        # Hotfix: merge with words to get the sentence id
        words = self.setup.load("words")
        comprises = self.setup.load("comprises")
        m = words.merge(comprises, on=["doc_id", "word_id"])
        
        res = {}
        for fold in folds_to_use:        
            fold_comprises = m[m["sentence_id"].isin(folds[fold]["test"])] \
                                [comprises.columns].drop_duplicates()
            res[fold] = compute_matches(
                            words,
                            fold_comprises,
                            self.setup.load("predicted_ann_concepts", fold=fold),
                            self.setup.load("predicted_comprises", 
                                            unit_of_analysis=unit_of_analysis, 
                                            fold=fold),
                            self.setup.load("predicted_pred_concepts",
                                            unit_of_analysis=unit_of_analysis,
                                            fold=fold
                                           ),
                            match_type=match_type
                        )
            
        return res


class NativeModelPipeline(ModelPipeline):
    TAGGING_FORMATS = {
        "biohd": BIOHD,
        "biohd1234": BIOHD1234,
        "fuzzybio": FuzzyBIO,
        "bioo": BIOO
    }
    
    def __init__(self, setup):
        super().__init__(setup)
        
    def process_output(self, folds_to_use=range(0, 10, 2), 
                       unit_of_analysis="sentence"):
        output = {}
        simplify_composite_func = split_composite_entity()
        for fold in folds_to_use:
            print("FOLD", fold)
            print("Reconstructing entities...", end="\r")
            predictions = self.setup.load("predicted_tags", 
                                          unit_of_analysis=unit_of_analysis, 
                                          fold=fold) \
                                .reset_index() \
                                .merge(self.setup.load("words"))
            
            # Reconstruction is always done at sentence level
            p = predictions.groupby("sentence_id")[["word_id", "word_text", "tag"]].apply(lists)
            
            # Compile lists of words and predicted tags per sentence
            #p = g["word_id"].apply(list).rename("words").to_frame() \
            #        .join(g["tag"].apply(list).rename("tags"))      
            
            
            # Apply reconstruction procedure at the sentence level        
            t = tqdm(total=len(p), position=0, leave=True)
            def __reconstruct_entities(x):
                res = self.TAGGING_FORMATS[self.setup.tagging_format].reconstruct_entities(x["word_id_list"], 
                                                                                           x["tag_list"], 
                                                                                           x["word_text_list"],
                                                                                           simplify_composite_func)
                return res
            
            reconstructed_entities = p \
                .apply(__reconstruct_entities, axis=1) \
                .explode() \
                .dropna() \
                .rename("word_id") \
                .reset_index()
            reconstructed_entities["doc_id"] = reconstructed_entities["sentence_id"].apply(lambda x: x.split("-", maxsplit=1)[0])
            reconstructed_entities.drop(columns=["sentence_id"], inplace=True)
            reconstructed_entities["entity_id"] = reconstructed_entities \
                .groupby("doc_id").cumcount()
            reconstructed_entities = reconstructed_entities.explode("word_id")
            
            predicted_comprises = reconstructed_entities[["doc_id", "entity_id", "word_id"]]
            
            # Create top-level and composite IDs
            print("Generating depth representations...", end="\r")
            predicted_composes = generate_depth_representations(predicted_comprises)
            
            # Store to disk
            print("Storing to disk...                       ")
            self.setup.save("predicted_comprises")(predicted_comprises, 
                                                   unit_of_analysis=unit_of_analysis,
                                                   fold=fold
                                                  )
            self.setup.save("predicted_composes")(predicted_composes, 
                                                  unit_of_analysis=unit_of_analysis,
                                                  fold=fold
                                                 )
            
            output[fold] = {
                "predicted_comprises": predicted_comprises,
                "predicted_composes": predicted_composes,
            }
            
        return output
            

class Dai20ModelPipeline(ModelPipeline):
    DATA_FOLDER = "../data/external/dai20"
    OUTPUT_MENTION_REGEX = re.compile(r"(,?\d*)*(?= .)")
    
    def __init__(self, setup):
        super().__init__(setup)
    
    def preprocess_data(self, folds_to_use=range(0, 10, 2), 
                        unit_of_analysis="sentence"):
        # Convert data to the appropriate format for Dai
        words = self.setup.load("words")
        comprises = self.setup.load("comprises")
        
        print("Aligning tokenizations...")
        aligned_comprises, alignment = align_with_dai_tokens(words, comprises)
        inputs = convert_to_dai_format(aligned_comprises, alignment)
        
        # Filter fold data and store to disk
        print("Generating input files...")
        folds = self.setup.load("folds", unit_of_analysis=unit_of_analysis)
        save_dir = path.join(self.DATA_FOLDER, "input", self.setup.sample.dataset.name)
        for fold in folds_to_use:
            fold_save_dir = path.join(save_dir, str(fold))
            if not path.exists(fold_save_dir):
                makedirs(fold_save_dir)
            
            for split, dai_split in {"train": "train", "val": "dev", "test": "test"}.items():
                fold_inputs = inputs[inputs.index.isin(folds[fold][split])]
                
                # Store IDs of sentences to enable mapping after predictions
                fold_inputs.reset_index()["sentence_id"] \
                    .to_csv(path.join(fold_save_dir, "{}.id".format(dai_split)), 
                            header=False, index=False)

                with open(path.join(fold_save_dir, "{}.txt".format(dai_split)), "w") as f:
                    for _, (text, boundaries) in fold_inputs.iterrows():
                        f.write(text)
                        f.write("\n")
                        f.write("|".join(["{} ADR".format(",".join([str(b) for c in e for b in c])) for e in boundaries]) if boundaries == boundaries else "")
                        f.write("\n")
                        f.write("\n")
                        
        # Store aligned token IDs for use with predictions
        aligned_comprises.to_pickle(path.join(save_dir, "mappings.pkl"))
                        
        # Create output folders
        print("Creating output folders...")
        output_dir = path.join(self.DATA_FOLDER, "output", self.setup.sample.dataset.name)
        for fold in folds_to_use:
            fold_output_dir = path.join(output_dir, str(fold))
            if not path.exists(fold_output_dir):
                makedirs(fold_output_dir)
                
        print("Success!")
        
        return inputs
    
    def process_output(self, folds_to_use=range(0, 10, 2), 
                        unit_of_analysis="sentence"):        
        input_dir = path.join(self.DATA_FOLDER, "input", self.setup.sample.dataset.name)
        output_dir = path.join(self.DATA_FOLDER, "output", self.setup.sample.dataset.name)
        
        print("Loading tokenization mappings...")
        aligned_comprises = pd.read_pickle(path.join(input_dir, "mappings.pkl"))
        
        print("Parsing model output...")
        output = {}
        for fold in folds_to_use:
            print("\rFOLD ", fold)
            # Load sentence IDs (so as to map them to the predictions)
            sentence_ids = pd.read_csv(path.join(input_dir, str(fold), "test.id"), 
                                       index_col=0, names=["sentence_id"], header=None)
            
            ps = self.parse_output(output_dir, fold)
            
            print("Converting to Pandas objects...", end="\r")
            s = pd.Series(ps, index=sentence_ids.index).rename("dai_id").dropna()
            
            # Generate an ID for each entity (has to be unique across doc_id)
            e = s.explode().reset_index()
            e = e.merge(aligned_comprises[["doc_id", "sentence_id"]].drop_duplicates())
            e["r_id"] = e.groupby("doc_id").cumcount()
            
            # Merge with comprises to retrieve word IDs according to our tokenization
            predicted_comprises = aligned_comprises \
                .merge(e.explode("dai_id").reset_index(), on=["doc_id", "sentence_id", "dai_id"]) \
                [["doc_id", "word_id", "r_id"]].drop_duplicates() \
                .rename(columns={"r_id": "entity_id"})
            
            # Create top-level and composite IDs
            print("Generating depth representations...", end="\r")
            predicted_composes = generate_depth_representations(predicted_comprises)
            
            # Store to disk
            print("Storing to disk...                       ")
            self.setup.save("predicted_comprises")(predicted_comprises, 
                                                   unit_of_analysis=unit_of_analysis,
                                                   fold=fold
                                                  )
            self.setup.save("predicted_composes")(predicted_composes, 
                                                  unit_of_analysis=unit_of_analysis,
                                                  fold=fold
                                                 )
            
            output[fold] = {
                "predicted_comprises": predicted_comprises,
                "predicted_composes": predicted_composes,
            }
            
        print("Success!")
            
        return output
    
    def parse_output(self, output_dir, fold):
        # Parse predictions from Dai output file
        ps = []
        with open(path.join(output_dir, str(fold), "test.pred"), "r") as f:
            for ln, line in enumerate(f):
                if ln%3 == 1: # The second line contains the predictions 
                    pred = line.strip()
                    if len(pred) > 0:
                        sentence_entities = []
                        for e in line.strip().split("|"):  # For each entity prediction
                            # Retrieve the list of boundaries from the mention
                            boundaries = [int(i) for i in self.OUTPUT_MENTION_REGEX.match(e)[0].split(",")]

                            # Split into pairs of two representing the boundaries of each chunk
                            chunks = np.reshape(boundaries, (int(len(boundaries)/2), 2)).tolist()

                            # Get the full list of tokens between the boundaries of each chunk
                            tokens = [t for i1, i2 in chunks for t in list(range(i1, i2+1, 1))]

                            sentence_entities.append(tokens)
                        ps.append(sentence_entities)
                    else:
                        ps.append(None)
        
        return ps
      
        
class Li21ModelPipeline(Dai20ModelPipeline):
    DATA_FOLDER = "../data/external/li21"
    
    def __init__(self, setup):
        super().__init__(setup)
    
    def preprocess_data(self, folds_to_use=range(0, 10, 2),
                        unit_of_analysis="sentence"
                       ):
         # Convert data to the appropriate format for Dai
        words = self.setup.load("words")
        comprises = self.setup.load("comprises")
        
        print("Aligning tokenizations for Dai...")
        aligned_comprises, alignment = align_with_dai_tokens(words, comprises)
        dai_inputs = convert_to_dai_format(aligned_comprises, alignment)       
        
        print("Preprocessing Dai data for Li...")
        inputs = generate_li_input_entries(dai_inputs)
        
        # Filter fold data and store to disk
        print("Generating input files...")
        folds = self.setup.load("folds", unit_of_analysis=unit_of_analysis)
        save_dir = path.join(LI_DATA_FOLDER, "input", self.setup.sample.dataset.name)
        for fold in folds_to_use:
            fold_save_dir = path.join(save_dir, str(fold))
            if not path.exists(fold_save_dir):
                makedirs(fold_save_dir)
                        
            for split, dai_split in {"train": "train", "val": "dev", "test": "test"}.items():
                fold_inputs = inputs[inputs.index.isin(folds[fold][split])]
                
                # Store IDs of sentences to enable mapping after predictions
                fold_inputs.reset_index()["sentence_id"] \
                    .to_csv(path.join(fold_save_dir, "{}.id".format(dai_split)), 
                            header=False, index=False)
                
                with open(path.join(fold_save_dir, "{}.json".format(dai_split)), 'w',
                          encoding='utf-8') as f:
                    for r in fold_inputs.to_dict("records"):
                        json.dump(r, f, ensure_ascii=False)
                        f.write("\n")
                        
        # Store aligned token IDs for use with predictions
        aligned_comprises.to_pickle(path.join(save_dir, "mappings.pkl"))
        
        # Create output folders
        print("Creating output folders...")
        output_dir = path.join(LI_DATA_FOLDER, "output", self.setup.sample.dataset.name)
        for fold in folds_to_use:
            fold_output_dir = path.join(output_dir, str(fold))
            if not path.exists(fold_output_dir):
                makedirs(fold_output_dir)
                
        print("Success!")
        
        return inputs
    
    def parse_output(self, output_dir, fold):
        # Parse predictions from Li output file
        ps = []
        with open(path.join(output_dir, str(fold), "predictions.txt"), "r") as f:
            for ln, line in enumerate(f):
                p = json.loads(line)[0]
                components = p["ner"]
                relations = p["relation"]
                boundaries = convert_chunks_relations_to_boundaries(components, relations)

                entities = []
                for entity in boundaries:
                    tokens = set()
                    for component in entity:
                        tokens = tokens | set(range(component[0], component[1]+1))
                    tokens = list(tokens)
                    tokens.sort()
                    entities.append(tokens)

                ps.append(entities)
            
        return ps

    
class EvaluationPipeline:
    POSITIVE_CLASSES = {
        "boundary": {
            "exact": ["exact"],
            "full": ["exact", "fuzzified", "padded"],
            "1-on-1": ["exact", "fuzzified", "padded", "partial"]
        },
        "concept": {
            "top-5": ["top-1", "top-5"]
        }
    }
    NUM_BOOTSTRAP_ITERATIONS = 1000
    PRECOMPUTATIONS = [
        ("boundary", "exact", None),
        ("boundary", "full"),
        ("boundary", "1-on-1"),
        ("concept", "top-5", None)
    ]
    DB_PATH = "../nb/checkpts/evaluation/cache.pkl"
    
    def __init__(self, setups):
        self.setups = { s: self.__load_setup_data(s) for s in setups }
        
        if path.exists(self.DB_PATH):
            with open(self.DB_PATH, "rb") as f: 
                self.cached = pickle.load(f)
        else:
            self.cached = {}
        self.cached.update({ s.get_display_name(): {} for s in setups if s.get_display_name() not in self.cached })
        
    def __dump_cache(self):
        if not path.exists(path.dirname(self.DB_PATH)):
            makedirs(path.dirname(self.DB_PATH))
        
        with open(self.DB_PATH, "wb") as f:
            pickle.dump(self.cached, f)
    
    def add_setup(self, setup):
        self.setups[setup] = self.load_setup_data(setup)
        if setup.get_display_name() not in self.cached:
            self.cached[setup.get_display_name()] = {}
        
    def precompute(self, precomputations=None, recompute=False):
        precomputations = self.PRECOMPUTATIONS if precomputations is None else precomputations
        
        t = tqdm(total=self.__count_required_computations(precomputations),
                 position=0, leave=True
                )
        for pc in precomputations:
            self.score(*pc, recompute=recompute, print_res=False, tqdm_obj=t, 
                       store_pickled=True)
        t.close()
        
        #self.__dump_cache()

    def score(self, match_type, positive_class, attribute=None, 
              recompute=False, print_res=True, print_ci=True, 
              tqdm_obj=None, store_pickled=False):
        attribute_list = [attribute] if isinstance(attribute, str) or attribute is None else attribute
        
        if tqdm_obj is None:
            t = tqdm(total=self.__count_required_computations([(match_type, positive_class, attribute)]),
                     position=0, leave=True)
        else:
            t = tqdm_obj

        for s, data in self.setups.items():
            if (match_type, positive_class, attribute) not in self.cached[s.get_display_name()] or recompute:
                m = data["true_span_matches"]
                p = data["predicted_spans"]
                recalled_true = m[m["match_status_{}".format(match_type)]
                                  .isin(self.POSITIVE_CLASSES[match_type][positive_class])]
                true_positives = recalled_true.reset_index("entity_id", drop=True) \
                                    [["matched_to_{}".format(match_type), "fold"]] \
                                    .explode("matched_to_{}".format(match_type)) \
                                    .dropna().reset_index().drop_duplicates() \
                                    .rename(columns={"matched_to_{}".format(match_type): "entity_id"}) \
                                    .set_index(["doc_id", "sentence_id", "entity_id", "fold"]) \
                                    .join(p.set_index("fold", append=True)) \
                                    .reset_index("fold")
                g = ["fold"]
                if attribute is not None:
                    g += attribute_list
                recall = self.__apply_bootstrap(recalled_true.groupby(g).apply(len) / m.groupby(g).apply(len))
                precision = self.__apply_bootstrap(true_positives.groupby(g).apply(len) / p.groupby(g).apply(len))
                self.cached[s.get_display_name()][(match_type, positive_class, attribute)] = {
                    "precision": precision,
                    "recall": recall,
                    "f1-score": 2 * (recall * precision) / (recall + precision)
                }
                t.update()
        
        if tqdm_obj is None:
            t.close()
            
        if store_pickled:
            self.__dump_cache()
            
        if print_res:
            ms = ["precision", "recall", "f1-score"]
            title_format = "{:<70} {}"
            groups = [("all",)] if attribute is None else [tuple(attr_set) for attr_set in self.__load_attributes()[list(attribute_list)].drop_duplicates().values]
            for g in groups:
                print("GROUP", g)
                print(title_format.format("model", "".join(["{:<23}".format(m) for m in ms])))
                for s in self.setups:
                    res = self.cached[s.get_display_name()][(match_type, positive_class, attribute)]
                    if not print_ci:
                        c = "".join(["{:<23.3f}".format(res[m].loc[g + ("mean_median",)]) for m in ms])
                    else:
                        scores = []
                        if (len(attribute_list) == 1 and g in res["precision"].index) or g in res["precision"].reset_index(-1).index:
                            c = "".join(["{:<5.3f} [{:<5.3f}-{:<5.3f}]    ".format(res[m].loc[g + ("mean_median",)],
                                                                                   res[m].loc[g + ("mean_lower_ci",)],
                                                                                   res[m].loc[g + ("mean_upper_ci",)]
                                                                                  ) for m in ms])
                        else:
                            c = "".join(["-.--- [-.--- - -.---]    " for m in ms])                            
                            
                    print(title_format.format(s.get_display_name(), c))
                print("\n")
            
    def compute_correlations(self, match_type, positive_class, attribute):
        # Compute point biserial correlation and p-value
        res = {}
        for s, data in self.setups.items():
            m = data["true_span_matches"].copy()

            # Convert match status to bit integer
            m["match_status_bit"] = 0
            m.loc[m["match_status_{}".format(match_type)]
                  .isin(self.POSITIVE_CLASSES[match_type][positive_class]),
                  "match_status_bit"] = 1
            correlations = m.groupby("fold") \
                .apply(lambda s: pd.Series(pointbiserialr(s["match_status_bit"],
                                                          s[attribute]), 
                                           index=["correlation", "p-value"]))
            res[s] = correlations
            
        # Print results
        title_format = "{:<60} {}"
        print(title_format
              .format("model", "".join(["{:<16}".format(f) 
                                        for f in list(range(0, 10, 2)) + ["all"]])))
        for s in self.setups:
            correlations = res[s]
            c = "".join(["{:<5.3f} ({:<5.3f})   ".format(correlations.loc[f, "correlation"],
                                                          correlations.loc[f, "p-value"])
                         for f in range(0, 10, 2)]
                        + ["{:<5.3f}    ".format(correlations["correlation"].mean())]
                       )
            print(title_format.format(s.get_display_name(), c))
         
    def __count_required_computations(self, computations):
        return len([(s, c) for s in self.setups for c in computations if c not in self.cached[s.get_display_name()]])        
    
    
    @staticmethod
    def __load_attributes():
        from setups import Dataset, Sample
        
        attrs = []
        for dataset in ["cadec", "psytar"]:
            d = Dataset(dataset)
            s = Sample("full", d)
            a = d.load("entity_attributes", depth="individual")
            fa = []
            for fold in range(0, 10, 2):
                fa.append(s.load("fold_entity_attributes", depth="individual", fold=fold))
            a = a.join(pd.concat(fa, axis=0))
            attrs.append(a)
        return pd.concat(attrs, axis=0)
    
    @staticmethod
    def __load_setup_data(setup, folds_to_use=range(0, 10, 2),
                          depth="individual",
                          unit_of_analysis="sentence"
                         ):
        true_span_matches = []
        predicted_spans = []
        for fold in folds_to_use:
            kwargs = dict(fold=fold, depth=depth, 
                          unit_of_analysis=unit_of_analysis)
            
            # Load matches
            m = setup.load("matches", **kwargs) \
                    .join(setup.load("concept_matches", **kwargs), 
                          lsuffix="_boundary",
                          rsuffix="_concept"
                         )
            m["fold"] = fold
            
            # Append entity attributes
            attrs = setup.load("entity_attributes", depth="individual") \
                        .join(setup.load("fold_entity_attributes", 
                                         depth=depth, 
                                         unit_of_analysis=unit_of_analysis, 
                                         fold=fold))
            m = m.join(attrs, how="left")
            assert len(m[m["is_discontinuous"].isna()]) == 0
            
            true_span_matches.append(m)
        
            
            # Load predicted spans
            m = setup.load("words") \
                    .merge(setup.load("predicted_comprises", **kwargs)) \
                    [["doc_id", "sentence_id", "entity_id"]] \
                    .drop_duplicates() \
                    .set_index(["doc_id", "sentence_id", "entity_id"])
            m["fold"] = fold
            
            # Append entity attributes
            attrs = setup.load("predicted_entity_attributes", 
                               unit_of_analysis=unit_of_analysis, 
                               fold=fold, 
                               depth=depth)
            m = m.join(attrs, how="left")
            assert len(m[m["is_discontinuous"].isna()]) == 0
            
            predicted_spans.append(m)
            
        return {
            "true_span_matches": pd.concat(true_span_matches),
            "predicted_spans": pd.concat(predicted_spans)
        }
    
    @classmethod
    def __apply_bootstrap(cls, d):
        g = (lambda x: "all") if len(d.index.names) == 1 else [a for a in d.index.names if a != "fold"]
        return d.rename("score") \
                .reset_index().groupby(g) \
                ["score"] \
                .apply(lambda s: pd.Series(cls.__bootstrap(s), 
                                           index=["mean",
                                                  "std",
                                                  "mean_lower_ci",
                                                  "mean_median",
                                                  "mean_upper_ci"
                                                 ]
                                          ))
                             
    
    @classmethod
    def __bootstrap(cls, values):
        n = len(values)

        means = []
        for i in range(cls.NUM_BOOTSTRAP_ITERATIONS):
            sample = resample(values, random_state=i)
            means.append(np.mean(sample))

        res = {["mean_lower_ci", "mean_median", "mean_upper_ci"][i]: v for i, v in enumerate(np.percentile(means, [2.5, 50, 97.5]))}
        res["mean"] = np.mean(values)
        res["std"] = np.std(values)

        return res["mean"], res["std"], res["mean_lower_ci"], res["mean_median"], res["mean_upper_ci"]      
    