from os import path, makedirs
import pandas as pd

from abc import ABC, abstractmethod

RAW_DATA_DIR = "../data/raw"
DATA_DIR = "../data/standardized"
FOLD_DIR = "../data/folds"
TAGS_DIR = "../data/tagged"
LR_RANGE_DIR = "../nb/checkpts/lr_range_tests"
TUNER_RESULT_DIR = "../nb/checkpts/hp_tuning"
MODEL_CHECKPOINT_DIR = "../nb/checkpts/model_checkpoints"
PREDICTOR_SAVE_DIR = "../nb/checkpts/predictors"
PREDICTIONS_DIR = "../nb/checkpts/predictions"
PREDICTED_ENTITIES_DIR = "../nb/checkpts/predicted_entities"
MATCHES_DIR = "../nb/checkpts/matches"

SCT_SAVE_PATH = "../data/sct/concepts_synonyms.pkl"

TAGGING_SCHEMES = [
    "biohd",
    "biohd1234",
    "fuzzybio",
    "bioo"
]

SEQUENCE_TAGGERS = [
    "ktrain_bilstm_bert",
    "ktrain_bilstm_bert_weightedcce",
    "ktrain_bilstm-crf-elmo",
    "hf_bert-base"
]

UNITS_OF_ANALYSIS = [
#    "doc",
    "sentence"
]

OVERLAP_REPRESENTATIONS = {
    "individual": "entity_id",
    "flattened": "flattened_id",
    "composite": "composite_id"
}

MATCH_RESULTS = ["exact", "padded", "partial", "broken", "absorbed", "missed"]

def format_path(path_table, name, **kwargs):
    if name not in path_table:
        return ""
    p, v = path_table[name]
    return p.format(*[kwargs[f] for f in v])


class PathLoader(ABC):
    @abstractmethod
    def load_path(self, table, **kwargs):
        pass

    def load(self, table, **kwargs):           
        return pd.read_pickle(self.load_path(table, **kwargs))
    
    def save(self, table):
        def store_data(data, **kwargs):
            p = self.load_path(table, **kwargs)
            
            if not path.exists(path.dirname(p)):
                makedirs(path.dirname(p))
            
            data.to_pickle(p)            
        return store_data

class Dataset(PathLoader):
    def __init__(self, name):
        self.name = name
        self.__paths = {
            "words": (path.join(DATA_DIR, name, "words.pkl"), []),
            "entities": (path.join(DATA_DIR, name, "entities.pkl"), []),
            "comprises": (path.join(DATA_DIR, name, "comprises.pkl"), []),
            "composes": (path.join(DATA_DIR, name, "composes.pkl"), []),
            "entity_attributes": (path.join(DATA_DIR, name, "entity_attributes_{}.pkl"), ["depth"]),
            "tagged": (path.join(TAGS_DIR, name, "{}.pkl"), ["tagging_format"])
        }
        
    def load_path(self, table, **kwargs):
        return format_path(self.__paths, table, **kwargs)
    
    def get_round_trip_dir(self, overlap_representation, tagging_scheme):
        return path.join(MATCHES_DIR, self.name, "full", "round_trip", overlap_representation, tagging_scheme)
    
    
class Sample(PathLoader):
    def __init__(self, name, dataset):
        self.name = name
        self.dataset = dataset
        self.__paths = {
            "folds": (path.join(FOLD_DIR, self.dataset.name, self.name, "{}s.pkl"), ["unit_of_analysis"]),
            "fold_entity_attributes": (path.join(FOLD_DIR, self.dataset.name, self.name, "entity_attributes", "{}", "{}.pkl"), ["fold", "depth"]),
            "predicted_ann_concepts": (path.join(FOLD_DIR, self.dataset.name, self.name, "concepts", "{}.pkl"), ["fold"]),
        }
        
    def load_path(self, table, **kwargs):
        p = format_path(self.__paths, table, **kwargs)
        if p == "":
            return self.dataset.load_path(table, **kwargs)
        return p
    
class Model(PathLoader):
    def __init__(self, name, sample, tagging_format="default"):
        self.name = name
        self.sample = sample
        self.tagging_format = tagging_format
        self.__paths = {
            "lr_range_plot": (path.join(sample.dataset.name, 
                                        sample.name,
                                        name,
                                        tagging_format,
                                        "{}",
                                        "lr_range_test.pdf"
                                       ), ["unit_of_analysis"]),
            
            "predicted_tags": (path.join(PREDICTIONS_DIR, 
                                         sample.dataset.name,
                                         sample.name,
                                         name,
                                         tagging_format, 
                                         "{}",
                                         "{}.pkl"
                                        ), ["unit_of_analysis", "fold"]),
            
            "predicted_comprises": (path.join(PREDICTED_ENTITIES_DIR, 
                                              sample.dataset.name,
                                              sample.name,
                                              name,
                                              tagging_format,
                                              "{}",
                                              "{}",
                                              "comprises.pkl"
                                             ), ["unit_of_analysis", "fold"]),
            
            "predicted_composes": (path.join(PREDICTED_ENTITIES_DIR, 
                                             sample.dataset.name,
                                             sample.name,
                                             name,
                                             tagging_format,
                                             "{}",
                                             "{}",
                                             "composes.pkl"
                                            ), ["unit_of_analysis", "fold"]),
            
            "predicted_entity_attributes": (path.join(PREDICTED_ENTITIES_DIR, 
                                                      sample.dataset.name,
                                                      sample.name,
                                                      name,
                                                      tagging_format,
                                                      "{}",
                                                      "{}",
                                                      "entity_attributes_{}.pkl"
                                                     ), ["unit_of_analysis", "fold", "depth"]),
            
            "predicted_pred_concepts": (path.join(PREDICTED_ENTITIES_DIR, 
                                             sample.dataset.name,
                                             sample.name,
                                             name,
                                             tagging_format,
                                             "{}",
                                             "{}",
                                             "concepts.pkl"
                                            ), ["unit_of_analysis", "fold"]),
            
            "matches": (path.join(MATCHES_DIR, 
                                  sample.dataset.name,
                                  sample.name,
                                  name,
                                  "{}",
                                  tagging_format, 
                                  "{}",
                                  "boundary",
                                  "{}.pkl"
                                ), ["depth", "unit_of_analysis", "fold"]),           
            
                        
            "concept_matches": (path.join(MATCHES_DIR, 
                                  sample.dataset.name,
                                  sample.name,
                                  name,
                                  "{}",
                                  tagging_format, 
                                  "{}",
                                  "concept",
                                  "{}.pkl"
                                ), ["depth", "unit_of_analysis", "fold"]),   
        }
            
    def load_path(self, table, **kwargs):
        p = format_path(self.__paths, table, **kwargs)
        if p == "":
            return self.sample.load_path(table, **kwargs)
        return p
    
    def get_display_name(self):
        return "{}_{}_{}_{}".format(self.sample.dataset.name,
                                    self.sample.name,
                                    self.name,
                                    self.tagging_format
                                   )
    
    def get_full_model_name(self):
        return "{}_{}".format(self.name, self.tagging_format)
    
    def get_model_checkpoint_path(self, unit_of_analysis, fold):
        return path.join(MODEL_CHECKPOINT_DIR, 
                         self.sample.dataset.name,
                         self.sample.name,
                         self.name,
                         self.tagging_format, 
                         unit_of_analysis,
                         str(fold)
                        )
    
    def get_tuner_save_dir(self, unit_of_analysis, fold):
        return path.join(TUNER_RESULT_DIR, 
                         self.sample.dataset.name,
                         self.sample.name,
                         self.name,
                         self.tagging_format, 
                         unit_of_analysis,
                         str(fold)
                        )
    
    def get_predictor_save_dir(self, unit_of_analysis, fold):
        return path.join(PREDICTOR_SAVE_DIR, 
                         self.sample.dataset.name,
                         self.sample.name,
                         self.name,
                         self.tagging_format, 
                         unit_of_analysis,
                         str(fold)
                        )
    