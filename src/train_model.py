import os
import argparse
import io
import random

import numpy as np
from tqdm import tqdm

from pandas_helpers import lists
from setups import Dataset, Sample, Model


def train(dataset, tagging_formats, folds, cuda_device):
    print("Configuring CUDA...")
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = cuda_device
    
    import tensorflow as tf
    from models.training import ModelTrainer

    def reset_random_state():
        random_seed = 0
        random.seed(random_seed)
        np.random.seed(random_seed)
        tf.random.set_seed(random_seed)    
    
    # Load pretrained word embeddings (GloVe)
    print("Loading pretrained word vectors...")
    EMBEDDING_DIM = 100
    EMBEDDING_SIZE = 400000

    def load_vectors(fname):
        fin = io.open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore')
        data = {}
        for line in tqdm(fin, total=EMBEDDING_SIZE):
            tokens = line.rstrip().split(' ')
            data[tokens[0]] = list(map(float, tokens[1:]))
        return data

    word_vectors = load_vectors("/data/s3386473/lit/repl/dai20/glove/glove.6B.100d.txt")
    
    MODEL_NAME = "native"
    UNIT_OF_ANALYSIS = "sentence"
    TAGS = {
        "biohd": ["B-ADR", "I-ADR", "O", "HB-ADR", "HI-ADR", "DB-ADR", "DI-ADR"],
        "biohd1234": ["B-ADR", "I-ADR", "O", "HB-ADR", "HI-ADR", "D1B-ADR", "D1I-ADR", "D2B-ADR", "D2I-ADR", "D3B-ADR", "D3I-ADR", "D4B-ADR", "D4I-ADR"],
        "fuzzybio": ["B-ADR", "I-ADR", "O"],
        "bioo": ["B-ADR", "I-ADR", "O", "INB-ADR"],
    }

    # Prepare word vectors for initialization
    print("Preparing word to id mappings...")
    sample = Sample("full", Dataset(dataset))
    word_vocab = sample.load("words")["word_text"].str.lower() \
        .drop_duplicates().rename("word").to_frame()
    word_vocab["vector"] = word_vocab["word"].map(lambda w: word_vectors.get(w, [0] * EMBEDDING_DIM))
    word_vocab.index = range(len(word_vocab))
    initial_embedding_weights = np.array([[0] * EMBEDDING_DIM] + list(word_vocab["vector"].values))
    word2id = {k: i+1 for i, k in word_vocab["word"].items()}

    # Build character vocabulary
    print("Preparing char to id mappings...")
    char_vocab = word_vocab["word"].apply(list).explode().drop_duplicates()
    char_vocab.index = range(len(char_vocab))
    char2id = {k: i+1 for i,k in char_vocab.items()}

    print("Start training...")
    for tagging_format in tagging_formats:
        print("TAGGING FORMAT:", tagging_format)
        setup = Model(MODEL_NAME, sample, tagging_format=tagging_format)
        tags = TAGS[tagging_format]

        # Prepare trainer
        model_trainer = ModelTrainer(tags=tags, word2id=word2id, 
                                     initialized_word_embeddings=initial_embedding_weights,
                                     char2id=char2id,  
                                     custom_name=MODEL_NAME, verbose=True)

        # Load data and get texts and labels
        data = setup.load("tagged", tagging_format=tagging_format)
        fold_dict = setup.load("folds", unit_of_analysis=UNIT_OF_ANALYSIS)

        # Preprocess data
        for fold in folds:
            print("FOLD", fold)

            # Preprocess data
            print("Preprocessing data...")
            fold_data = {}
            for split in ["train", "val", "test"]:
                split_data = data[data["sentence_id"].isin(fold_dict[fold][split])] \
                            .groupby("sentence_id")[["word_id", "word_text", "tag"]].apply(lists)
                fold_data["{}_{}".format(split, "doc_ids")] = split_data.index
                fold_data["{}_{}".format(split, "word_ids")] = split_data["word_id_list"]
                fold_data["{}_{}".format(split, "texts")] = split_data["word_text_list"].tolist()
                fold_data["{}_{}".format(split, "tags")] = split_data["tag_list"].tolist()

            preprocessed_data = model_trainer.preprocess_data(**fold_data)

            reset_random_state()

            # Train model
            print("Training model...")
            model, history = model_trainer.fit(preprocessed_data["train"], 
                                               preprocessed_data["val"],
                                               checkpoint_save_path=setup.get_model_checkpoint_path(UNIT_OF_ANALYSIS, fold),
                                               model_save_path=setup.get_predictor_save_dir(UNIT_OF_ANALYSIS, fold)
                                              )

            reset_random_state()
            # Make predictions on test set
            print("Making predictions...")
            predictions = model_trainer.predict(preprocessed_data["test"],
                                                model_load_path=setup.get_predictor_save_dir(UNIT_OF_ANALYSIS, fold)
                                               )

            # Store predictions to disk
            print("Saving predictions to disk...")
            setup.save("predicted_tags")(predictions, unit_of_analysis=UNIT_OF_ANALYSIS, fold=fold)

    print("Success!")    

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", type=str)
    parser.add_argument("--tagging_formats", type=str, nargs="+", required=True)
    parser.add_argument("--folds", type=int, nargs="+", required=True)
    parser.add_argument("--cuda", type=str, required=True)
    
    args = parser.parse_args()
    
    train(args.dataset, args.tagging_formats, args.folds, args.cuda)
    