import sys
sys.path.append("../src")

import pandas as pd

from pandas_helpers import lists

PARTIAL_MATCH_THRESHOLD = 0

def compute_matches(words,
                    true_comprises,
                    true_concepts,
                    pred_comprises,
                    pred_concepts,
                    match_type
                   ):
    true_spans = __consolidate_spans(words, true_comprises, true_concepts) \
                        .rename(columns={
                                            "entity_id": "true_id",
                                            "words": "true_words",
                                            "cui_id": "true_cui_id"
                                        })
    pred_spans = __consolidate_spans(words, pred_comprises, pred_concepts) \
                        .rename(columns={
                                            "entity_id": "pred_id", 
                                            "words": "pred_words",
                                            "cui_id": "pred_cui_id"
                                        })
    
    match_func = find_boundary_matches if match_type == "boundary" else find_concept_matches
    
    matches = true_spans.groupby(["doc_id", "sentence_id"]).apply(lists).join(
                    pred_spans.groupby(["doc_id", "sentence_id"]).apply(lists), how="left"
                ) \
                .apply(lambda s: match_func((s["true_id_list"], 
                                             s["true_words_list"],
                                             s["true_cui_id_list"]
                                            ), 
                                            (s["pred_id_list"], 
                                             s["pred_words_list"],
                                             s["pred_cui_id_list"]
                                            )), 
                       axis=1) \
                .explode(["entity_id", "match_status", "matched_to"]) \
                .set_index("entity_id", append=True)
    
    return matches

    
def find_boundary_matches(true_spans, pred_spans):
    true_span_ids, true_span_tokens = true_spans[:2]
    pred_span_ids, pred_span_tokens = pred_spans[:2]
    
    matches = {}
    
    # Do the check only if there are predicted spans for this sentence
    if pred_span_ids == pred_span_ids and pred_span_ids is not None:
        matched_pred_ids = set()
        
        # PASS 0: Find exact matches: there is a single reconstructed entity that matches the original entity exactly
        for true_id, true_words in zip(true_span_ids, true_span_tokens):
            for pred_id, pred_words in zip(pred_span_ids, pred_span_tokens):
                if true_words == pred_words:
                    matches[true_id] = {"match_status": "exact", "matched_to": [pred_id]}
                    matched_pred_ids.add(pred_id)
                    break
        
        # PASS 1: Find exact matches: there is a single reconstructed entity that matches the original entity exactly incl. intermediate tokens
        #for true_id, true_words in zip(true_span_ids, true_span_tokens):
        #    if true_id not in matches:
        #        for pred_id, pred_words in zip(pred_span_ids, pred_span_tokens):
        #            if pred_id not in matched_pred_ids:
        #                if set(range(min(true_words), max(true_words)+1)) == set(range(min(pred_words), max(pred_words)+1)):                        
        #                    matches[true_id] = {"match_status": "fuzzified", "matched_to": [pred_id]}
        #                    matched_pred_ids.add(pred_id)
        #                    break

        # PASS 2: Find padded matches: there is a single reconstructed entity that contains the original entity
        #for true_id, true_words in zip(true_span_ids, true_span_tokens):
        #    if true_id not in matches:
        #        for pred_id, pred_words in zip(pred_span_ids, pred_span_tokens):
        #            if pred_id not in matched_pred_ids:
        #                if len(true_words - pred_words) == 0:
        #                    matches[true_id] = {"match_status": "padded", "matched_to": [pred_id]}
        #                    matched_pred_ids.add(pred_id)
        #                    break

        # PASS 3: Find partial matches: there is a single reconstructed entity that partially matches the original entity
        for true_id, true_words in zip(true_span_ids, true_span_tokens):
            if true_id not in matches:
                for pred_id, pred_words in zip(pred_span_ids, pred_span_tokens):
                    if pred_id not in matched_pred_ids:
                        if len(true_words & pred_words) / len(true_words) > PARTIAL_MATCH_THRESHOLD:
                            matches[true_id] = {"match_status": "partial", "matched_to": [pred_id]}
                            matched_pred_ids.add(pred_id)
                            break
            
    # All other entities are considered as missed
    for true_id, true_words in zip(true_span_ids, true_span_tokens):
        if true_id not in matches:
            matches[true_id] = {"match_status": "missed", "matched_to": []}
    
    # Compile matching results
    res = {
        "entity_id": [],
        "match_status": [],
        "matched_to": []
    }
    
    for true_id in true_span_ids:
        res["entity_id"].append(true_id)
        match_info = matches[true_id]
        res["match_status"].append(match_info["match_status"])
        res["matched_to"].append(match_info["matched_to"])
 
    return pd.Series(res)


def find_concept_matches(true_spans, pred_spans):
    true_span_ids, true_span_tokens, true_span_concepts = true_spans
    pred_span_ids, pred_span_tokens, pred_span_concepts = pred_spans
    
    matches = {}
    
    # Do the check only if there are predicted spans for this sentence
    if pred_span_ids == pred_span_ids and pred_span_ids is not None:
        matched_pred_ids = set()

        # PASS 1: Find matches based on top-1 concept
        for true_id, true_words, true_concepts in zip(true_span_ids, true_span_tokens, true_span_concepts):
            best_match = None
            widest_overlap = 0
            for pred_id, pred_words, pred_concepts in zip(pred_span_ids, pred_span_tokens, pred_span_concepts):
                overlap_size = len(pred_words & true_words)
                if overlap_size > widest_overlap and true_concepts[0] == pred_concepts[0]:
                    widest_overlap = overlap_size
                    best_match = pred_id
            if best_match is not None:                    
                matches[true_id] = {"match_status": "top-1", "matched_to": [best_match]}
                matched_pred_ids.add(best_match)
        
        # PASS 2; Find matches based on top-5 concept
        for true_id, true_words in zip(true_span_ids, true_span_tokens):
            if true_id not in matches:
                best_match = None
                widest_overlap = 0
                for pred_id, pred_words in zip(pred_span_ids, pred_span_tokens):
                    if pred_id not in matched_pred_ids:
                        if overlap_size > widest_overlap and len(set(true_concepts) & set(pred_concepts)) > 0:
                            overlap_size = len(pred_words & true_words)
                            if overlap_size > widest_overlap and true_concepts[0] == pred_concepts[0]:
                                widest_overlap = overlap_size
                                best_match = pred_id
                    if best_match is not None:                    
                        matches[true_id] = {"match_status": "top-5", "matched_to": [best_match]}
                        matched_pred_ids.add(best_match)
    
    # All other entities are considered as missed
    for true_id, true_words in zip(true_span_ids, true_span_tokens):
        if true_id not in matches:
            matches[true_id] = {"match_status": "missed", "matched_to": []}
    
    # Compile matching results
    res = {
        "entity_id": [],
        "match_status": [],
        "matched_to": []
    }
    
    for true_id in true_span_ids:
        res["entity_id"].append(true_id)
        match_info = matches[true_id]
        res["match_status"].append(match_info["match_status"])
        res["matched_to"].append(match_info["matched_to"])
 
    return pd.Series(res)    
    

def __consolidate_spans(words, comprises, concepts):
    return words.drop(columns=["word_text"]) \
            .merge(comprises, on=["doc_id", "word_id"]) \
            [["doc_id", "sentence_id", "entity_id", "word_id"]] \
            .drop_duplicates() \
            .sort_values(["doc_id", "word_id"]) \
            .groupby(["doc_id", "sentence_id", "entity_id"]) \
            ["word_id"].apply(set).rename("words") \
            .to_frame().join(concepts, how="left") \
            .reset_index(level=2)
