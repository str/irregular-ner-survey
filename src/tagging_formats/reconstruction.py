MAX_INTERCEPTIONS = 0


class BIOHD:
    tags = {
        "I-ADR": "B-ADR",
        "HI-ADR": "HB-ADR",
        "DI-ADR": "DB-ADR"
    }
    
    @classmethod
    def reconstruct_entities(cls, word_ids, tags, *args):
        entities = []

        # STEP 1: CONSTRUCT ENTITY CHUNKS
        entity_chunks = []
        curr_chunk = {}
        intercepts = []

        for i in range(len(tags)):
            word_id = word_ids[i]
            tag = tags[i]

            if tag == "O":
                if len(curr_chunk) > 0:
                    if len(intercepts) < MAX_INTERCEPTIONS:
                        # If it is the first "O" we reach, do not yet finalize
                        # the current entity (A.1)
                        intercepts.append(word_id)
                    else:
                        # If maximum number of intercepting "O" tags has been reached,
                        # close the current entity (A.1)
                        entity_chunks.append(curr_chunk)
                        curr_chunk = {}
                        intercepts = []
            else:
                if tag in cls.tags:  # if an "I" tag
                    if len(curr_chunk) > 0:
                        # Entity can continue after a single "O" (B.1)
                        if len(intercepts) > 0:
                            curr_chunk["words"].extend(intercepts)
                        curr_chunk["words"].append(word_id)
                    else:
                        # Allow for an entity to open with "I" (B.3)
                        curr_chunk = {
                            "type": cls.tags[tag][:-4],
                            "words": [word_id]
                        }
                else:  # if a "B" tag
                    # Flush the current entity and open a new one (B.2)
                    if len(curr_chunk) > 0:
                        entity_chunks.append(curr_chunk)
                    curr_chunk = {
                        "type": tag[:-4],
                        "words": [word_id]
                    }

                intercepts = []

        # Flush curr_chunk
        if len(curr_chunk) > 0:
            entity_chunks.append(curr_chunk)

        # STEP 2: RE-CONSTRUCT IRREGULAR ENTITIES    
        chunk_types = [c["type"] for c in entity_chunks]
        if "HB" in chunk_types:
            # Separate combinations with each head entity
            for head_chunk in [c for c in entity_chunks if c["type"] == "HB"]:
                if "DB" not in chunk_types:
                    # If no "D" parts, list entity as a regular one (B.6)
                    entities.append(head_chunk["words"])
                else:
                    for disjoint_chunk in [c for c in entity_chunks if c["type"] == "DB"]:
                        e = head_chunk["words"] + disjoint_chunk["words"]
                        e.sort()
                        entities.append(e)        
        elif "DB" in chunk_types:
            # Assume a single discontinuous entity per sentence (A.2)
            discontinuous_entity = [wid for c in entity_chunks for wid in c["words"] if c["type"] == "DB"]
            discontinuous_entity.sort()
            entities.append(discontinuous_entity)

        # Regular entities are listed as-is
        for c in entity_chunks:
            if c["type"] == "B":
                entities.append(c["words"])

        entities.sort()

        return entities        

    
class BIOHD1234:
    tags = {
        "I-ADR": "B-ADR",
        "HI-ADR": "HB-ADR",
        "D1I-ADR": "D1B-ADR",
        "D2I-ADR": "D2B-ADR",
        "D3I-ADR": "D3B-ADR",
        "D4I-ADR": "D4B-ADR"
    }

    @classmethod
    def reconstruct_entities(cls, word_ids, tags, *args):
        entities = []

        # STEP 1: CONSTRUCT ENTITY CHUNKS
        entity_chunks = []
        curr_chunk = {}
        intercepts = []

        for i in range(len(tags)):
            word_id = word_ids[i]
            tag = tags[i]

            if tag == "O":
                if len(curr_chunk) > 0:
                    if len(intercepts) < MAX_INTERCEPTIONS:
                        # If it is the first "O" we reach, do not yet finalize
                        # the current entity
                        intercepts.append(word_id)
                    else:
                        # If maximum number of intercepting "O" tags has been reached,
                        # close the current entity
                        entity_chunks.append(curr_chunk)
                        curr_chunk = {}
                        intercepts = []
            else:
                if tag in cls.tags:  # if an "I" tag
                    if len(curr_chunk) > 0:
                        # Entity can continue after a single "O" (B.1)
                        if len(intercepts) > 0:
                            curr_chunk["words"].extend(intercepts)
                        curr_chunk["words"].append(word_id)
                    else:
                        # Allow for an entity to open with "I" (B.3)
                        curr_chunk = {
                            "type": cls.tags[tag][:-4],
                            "words": [word_id]
                        }
                else:  # if a "B" tag
                    # Flush the current entity and open a new one (B.2)
                    if len(curr_chunk) > 0:
                        entity_chunks.append(curr_chunk)
                    curr_chunk = {
                        "type": tag[:-4],
                        "words": [word_id]
                    }

                intercepts = []

        # Flush curr_chunk
        if len(curr_chunk) > 0:
            entity_chunks.append(curr_chunk)

        # STEP 2: RE-CONSTRUCT IRREGULAR ENTITIES

        latest_head = None
        latest_disjoint = None
        pending_d3 = []
        curr = {}

        for i, c in enumerate(entity_chunks):
            curr[i] = []
            if c["type"] == "B":
                curr[i].append(c["words"])
            elif c["type"] == "HB":
                # If previous head chunk was not linked to
                # any "D" entity, store it on its own
                if latest_head is not None and len(curr[latest_head["ind"]]) == 0:
                    curr[latest_head["ind"]].append(latest_head["words"])

                # Make this head chunk `latest`
                latest_head = {
                    "ind": i,
                    "words": c["words"]
                }

                # Combine with pending D3s
                for d3 in pending_d3:
                    for e in curr[d3]:
                        e.extend(c["words"])
                        curr[i].append(e)
                pending_d3 = []

            else:  # disjoint
                if c["type"] == "D1B":
                    if latest_head is not None:
                        # Combine with current head chunk, and make reference from both the head
                        # and the D1 chunk
                        combined = latest_head["words"] + c["words"]
                        curr[latest_head["ind"]].append(combined)
                        curr[i].append(combined)
                    else:
                        curr[i].append(c["words"])
                elif c["type"] == "D2B":
                    if latest_disjoint is not None:
                        for e in curr[latest_disjoint]:
                            e.extend(c["words"])
                            curr[i].append(e)
                    else:
                        curr[i].append(c["words"])
                elif c["type"] == "D3B":
                    curr[i].append(c["words"])
                    pending_d3.append(i)
                elif c["type"] == "D4B":
                    curr[i].append(c["words"])
                latest_disjoint = i

        if latest_head is not None and len(curr[latest_head["ind"]]) == 0:
            curr[latest_head["ind"]].append(latest_head["words"])

        [entities.append(e) for es in curr.values() for e in es if e not in entities]        
        return entities
    
    
class FuzzyBIO:
    
    @classmethod
    def reconstruct_entities(cls, word_ids, tags, texts, simplification_func):
        entities = []

        curr_chunk = []
        intercepts = []

        for i in range(len(tags)):
            word_id = word_ids[i]
            tag = tags[i]

            if tag == "O":
                if len(curr_chunk) > 0:
                    if len(intercepts) < MAX_INTERCEPTIONS:
                        # If it is the first "O" we reach, do not yet finalize
                        # the current entity
                        intercepts.append(word_id)
                    else:
                        # If maximum number of intercepting "O" tags has been reached,
                        # close the current entity
                        entities.append(curr_chunk)
                        curr_chunk = []
                        intercepts = []
            else:
                if tag == "I-ADR":
                    if len(curr_chunk) > 0:
                        # Entity can continue after a single "O"
                        if len(intercepts) > 0:
                            curr_chunk.extend(intercepts)
                        curr_chunk.append(word_id)
                    else:
                        # Allow for an entity to open with "I"
                        curr_chunk = [word_id]
                else:  # if a "B" tag
                    # Flush the current entity and open a new one (B.2)
                    if len(curr_chunk) > 0:
                        entities.append(curr_chunk)
                    curr_chunk = [word_id]

                intercepts = []

        # Flush curr_chunk
        if len(curr_chunk) > 0:
            entities.append(curr_chunk)
            
        # Simplify composite entities
        simplified_entities = []
        if len(entities) > 0:
            entity_texts = [[texts[word_ids.index(w)] for w in e] for e in entities]
            for word_ids, word_texts in zip(entities, entity_texts):
                simplified_entities.extend(simplification_func(word_ids, word_texts))

        return simplified_entities
    

class BIOO:
    @classmethod
    def reconstruct_entities(cls, word_ids, tags, texts, simplification_func):
        entities = []
        curr_chunk = []
        intercepts = []

        for i in range(len(tags)):
            word_id = word_ids[i]
            tag = tags[i]

            if tag == "O":
                if len(curr_chunk) > 0:
                    if len(intercepts) < MAX_INTERCEPTIONS:
                        # If it is the first "O" we reach, do not yet finalize
                        # the current entity
                        intercepts.append(word_id)
                    else:
                        # If maximum number of intercepting "O" tags has been reached,
                        # close the current entity
                        entities.append(curr_chunk)
                        curr_chunk = []
                        intercepts = []
            elif tag == "INB-ADR":
                pass
            else:
                if tag == "I-ADR":
                    if len(curr_chunk) > 0:
                        # Entity can continue after a single "O"
                        curr_chunk.append(word_id)
                    else:
                        # Allow for an entity to open with "I"
                        curr_chunk = [word_id]

                else:  # if tag is "B"
                    if len(curr_chunk) > 0:
                        entities.append(curr_chunk)   
                    curr_chunk = [word_id]             

                intercepts = []

        # Flush curr_chunk
        if len(curr_chunk) > 0:
            entities.append(curr_chunk)

        simplified_entities = []
            # Simplify composite entities
        if len(entities) > 0:
            entity_texts = [[texts[word_ids.index(w)] for w in e] for e in entities]
            for word_ids, word_texts in zip(entities, entity_texts):
                simplified_entities.extend(simplification_func(word_ids, word_texts))

        return simplified_entities  
    