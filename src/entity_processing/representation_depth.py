import sys
sys.path.append("../src")

from pandas_helpers import lists
from .array_utils import flatten_nested_entities, merge_overlapping_entities

DEPTH2ID = {
    "individual": "entity_id",
    "flattened": "flattened_id",
    "composite": "composite_id"
}

def generate_depth_representations(comprises):
    # Group entities and their words by doc
    entity_words = comprises.sort_values(["doc_id", "word_id"]) \
                        .groupby(["doc_id", "entity_id"])["word_id"].apply(list) \
                        .reset_index()
    doc_entities = entity_words.groupby("doc_id")[["entity_id", "word_id"]].apply(lists)

    # Flatten nested entities (Top-level representation)
    contains = doc_entities \
        .apply(lambda x: flatten_nested_entities(x["word_id_list"], x["entity_id_list"])[1],
               axis=1) \
        .explode().rename("entity_id").to_frame().reset_index()
    contains["flattened_id"] = contains.groupby("doc_id").cumcount()
    contains = contains.explode("entity_id")

    # Merge overlapping entities into composite entities
    doc_entities = entity_words \
                    .groupby("doc_id")[["entity_id", "word_id"]].apply(lists)
    composes = doc_entities \
        .apply(lambda x: merge_overlapping_entities(x["word_id_list"], x["entity_id_list"])[1],
              axis=1) \
        .explode().rename("entity_id").to_frame().reset_index()
    composes["composite_id"] = composes.groupby("doc_id").cumcount()
    composes = composes.explode("entity_id")

    return contains.merge(composes, on=["doc_id", "entity_id"]) 