import re
from tokenizers.pre_tokenizers import Whitespace


def is_discontinuous(words):
    """Returns whether an entity is discontinuous.
    
    Parameters:
    words (list): the list of word IDs in the entity
    
    Returns:
    bool: whether the entity is discontinuous
    """
    words = list(words)
    words.sort()
    for i in range(len(words)-1):
        if words[i+1] != words[i] + 1:
            return True
    return False


def extract_mention_chunks(words):
    """Returns a list of the continuous parts (word IDs) in an entity.
    If an entity is continuous, the list comprises a single element.
        
    Parameters:
    words (list): the list of word IDs in the entity
    
    Returns:
    list: a list of lists, each of which contains the word IDs of the
          continuous parts
    """
    chunks = []
    curr_chunk = []
    for w in words:
        if len(curr_chunk) > 0 and w != curr_chunk[-1] + 1:
            chunks.append(curr_chunk)
            curr_chunk = []
        curr_chunk.append(w)
    if len(curr_chunk) > 0:
        chunks.append(curr_chunk)
    return chunks


def flatten_nested_entities(words, ids=None):
    """
    Processes a series of entities in order to combine nested entities so that
    no entity is fully contained by another.
    
    If a nested entity is fully contained by an component where multiple non-nested
    entities overlap, then it is included in the first such entity to appear while
    traversing.
    
    Parameters:
    words (list of lists)    the word IDs that each entity contains
    ids (list of ints)       the IDs of the entities
    
    Returns:
    list of lists    the word IDs that each entity contains after the conversion
    list of ints     the entity IDs that correspond to each new entity 
    """
    # There is the option of passing entity IDs as an argument
    # in case the entities are not sorted, or the IDs do not follow
    # a continuous, monotonically increasing scheme
    if ids is None:
        ids = range(len(words))
    
    flattened = []
    
    for i, e in enumerate(words):
        matches = []
        
        # Among seen entities, check if there are any contained by or containing this one
        for c in flattened:
            if (set(e) & set(c[0])) == set(c[0]) or (set(e) & set(c[0])) == set(e):
                matches.append(c)
        
        # Remove matches from seen entities
        [flattened.remove(m) for m in matches]

        # Create a single entity out of all matches
        flattened_words = list(set(e).union(*[set(m[0]) for m in matches]))
        flattened_ids = [ids[i]] + [ind for m in matches for ind in m[1]]
        
        # Sorting is done for cosmetical reasons here
        flattened_words.sort()
        flattened_ids.sort()
        
        flattened.append((flattened_words, flattened_ids))
    
    return [c[0] for c in flattened], [c[1] for c in flattened]        


def merge_overlapping_entities(words, ids=None):
    """
    Processes a series of entities in order to merge overlapping entities
    into composite entities.
    
    Parameters:
    words (list of lists)    the word IDs that each entity contains
    ids (list of ints)       the IDs of the entities
    
    Returns:
    list of lists    the word IDs that each entity contains after the conversion
    list of ints     the entity IDs that correspond to each new entity 
    """
    
    # There is the option of passing entity IDs as an argument
    # in case the entities are not sorted, or the IDs do not follow
    # a continuous, monotonically increasing scheme
    if ids is None:
        ids = range(len(words))
    
    composites = []
    
    for i, e in enumerate(words):
        matches = []
        
        # Among seen entities, check if there are any with shared tokens
        for c in composites:
            if len(set(e) & set(c[0])) > 0:
                matches.append(c)
        
        # Remove so as to cover the following case:
        # Non-overlapping entities A and B have been seen so far,
        # and C overlaps with both. When C is checked, A and B are
        # removed, merged along with C and placed back as a single
        # composite
        [composites.remove(m) for m in matches]
        
        # Create a single entity out of all matches
        composite_words = list(set(e).union(*[set(m[0]) for m in matches]))
        composite_ids = [ids[i]] + [ind for m in matches for ind in m[1]]
        
        # Sorting is done for cosmetical reasons here
        composite_words.sort()
        composite_ids.sort()
        
        composites.append((composite_words, composite_ids))
    
    return [c[0] for c in composites], [c[1] for c in composites]


def check_overlap_location(all_words, shared_words):
    # Consider missing values as empty sets
    if shared_words != shared_words:
        shared_words = {}

    all_words = list(all_words)
    shared_words = list(shared_words)

    all_words.sort()
    shared_words.sort()

    if len(shared_words) == 0:
        return "none"
    elif all_words == shared_words or is_discontinuous(shared_words):
        return "multi"
    elif all(shared_words[i] == all_words[i] for i in range(len(shared_words))):
        return "left"
    elif all(shared_words[i] == all_words[len(all_words) - len(shared_words) + i] for i in range(len(shared_words))):
        return "right"
    else:
        return "inside"
    
    
def align_tokenizations(ids, words, other_ids, other_words):
    """Generates pairs of aligned spans between tokenizations.
    Example: [0], [2, 3] means that the token with ID 0 in the first tokenization corresponds
             to the concatenation of tokens with ID 2 and 3 in the second tokenization 
    """
    i = 0
    j = 0
    alignment = {
        "ids": [],
        "other_ids": []
    }
    
    curr = {
        "i": [],
        "j": [],
        "ids": [],
        "other_ids": []
    }
    
    curr["i"].append(0)
    curr["j"].append(0)
    
    while i < len(ids) and j < len(other_ids):
        word = "".join([words[i] for i in curr["i"]])
        other_word = "".join([other_words[j] for j in curr["j"]])
        
        # If currently compared token spans are aligned
        if word == other_word:
            alignment["ids"].append([ids[i] for i in curr["i"]])
            alignment["other_ids"].append([other_ids[j] for j in curr["j"]])
            curr = {k: [] for k in curr}
            
            # Move to next word
            i+=1
            curr["i"].append(i)
            j+=1
            curr["j"].append(j)
            
        elif len(word) > len(other_word) and word[:len(other_word)] == other_word:
            j+=1
            curr["j"].append(j)
            
        elif len(word) < len(other_word) and word == other_word[:len(word)]:
            i+=1
            curr["i"].append(i)
            
        else:
            print("Error!")
            break
    
    return alignment["ids"], alignment["other_ids"]

def split_composite_entity():
    tokenizer = Whitespace()

    def do(ids, words): 
        """ Adapted from https://github.com/dmis-lab/BioSyn/blob/02dd8c150521edc36dba3994dc103aefd76341ca/preprocess/query_preprocess.py#L120 """
        mention = " ".join(words).strip()

        prefix_of_pattern = re.compile(
            "(?P<prefix>[a-zA-Z-]+) of (both )?(the )?((my|your|his|her|our|their) ?)(?P<suffix1>([a-zA-Z-]+ )+)(and|or) (the )?(?P<suffix2>([a-zA-Z-]+ ?)+)"
        )
        prefix_in_pattern = re.compile(
            "(?P<prefix>[a-zA-Z-]+) in (both )?(the )?((my|your|his|her|our|their) ?)(?P<suffix1>([a-zA-Z-]+ )+)(and|or) (the )?(?P<suffix2>([a-zA-Z-]+ ?)+)"
        )
        nested_and_pattern = re.compile(
            "(?P<prefix_list>([a-zA-Z-]+,? )+)(and|or|and/or) (?P<prefix_last>[a-zA-Z-]+ and [a-zA-Z-]+) (?P<stem>.*)"
        )
        trivial_pattern = re.compile(
            "(?P<prefix_list>([a-zA-Z-]+,? )+)(and|or|and/or) (?P<prefix_last>(the )?[a-zA-Z-]+) (?P<stem>.*)"
        )
        slash_pattern = re.compile(
            "(?P<prefix>(.* )*)(?P<composite1>.+)\/(?P<composite2>.+)?(?P<suffix>( .*)*)"
        )
        prefix_of_match = prefix_of_pattern.match(mention)
        prefix_in_match = prefix_in_pattern.match(mention)
        nested_and_match = nested_and_pattern.match(mention)
        trivial_match = trivial_pattern.match(mention)
        slash_match = slash_pattern.fullmatch(mention)

        mention_list = []
        if prefix_of_match:
            match = prefix_of_match
            prefix = match.group('prefix')
            mention_list=[' '.join([prefix, 'of', match.group('suffix1').strip()]),
                          ' '.join([prefix, 'of', match.group('suffix2').strip()])]
        elif prefix_in_match:
            match = prefix_in_match
            prefix = match.group('prefix')
            mention_list=[' '.join([prefix, 'in', match.group('suffix1').strip()]),
                          ' '.join([prefix, 'in', match.group('suffix2').strip()])]
        elif nested_and_match:            
            match = nested_and_match
            stem = match.group('stem')
            prefix_list = match.group('prefix_list').strip().split(',')
            prefix_list.append(match.group('prefix_last'))
            prefix_list = filter(len, prefix_list)  # filter zero len prefix
            mention_list = [prefix.strip() + ' ' + stem for prefix in prefix_list]
        elif trivial_match:
            match = trivial_match
            stem = match.group('stem')
            prefix_list = match.group('prefix_list').strip().split(',')
            prefix_list.append(match.group('prefix_last'))
            prefix_list = filter(len, prefix_list)  # filter zero len prefix
            mention_list = [prefix.strip() + ' ' + stem for prefix in prefix_list]
        elif slash_match and 'and/or' not in mention:
            try:
                match = slash_match
                prefix = slash_match.group('prefix').strip()
                suffix = slash_match.group('suffix').strip()
                composite_list = [slash_match.group('composite1').strip(), slash_match.group('composite2').strip()]
                mention_list = [prefix + ' ' + composite + ' ' + suffix for composite in composite_list]
                mention_list = [mention.strip() for mention in mention_list]
            except:
                return [ids]
        else:
            return [ids]

        # Get token IDs
        word_ids = []
        for individual_mention in mention_list:
            mtokens = [t for t, _ in tokenizer.pre_tokenize_str(individual_mention)]
            possible_matches = []
            for mt in mtokens:
                token_matches = []
                for i, w in enumerate(words):
                    if mt == w:
                        token_matches.append(ids[i])
                possible_matches.append(token_matches)
        
            combinations = []
            for tid, token_matches in enumerate(possible_matches):
                if len(combinations) == 0:
                    combinations.extend([[m] for m in token_matches])
                else:
                    upd_combinations = []
                    for c in combinations:
                        for matched_id in token_matches:
                            cand_combination = c + [matched_id]
                            if all([c1 > c0 for i, c0 in enumerate(cand_combination[:-1]) for c1 in cand_combination[i+1:]]):
                                upd_combinations.append(cand_combination)
                    combinations = upd_combinations
            
            match_lengths = [sum([w1-w0 for i, w0 in enumerate(m) for w1 in m[i+1:]]) for m in combinations]
            min_length = min(match_lengths)
            matches = [m for i, m in enumerate(combinations) if match_lengths[i] == min_length]
            word_ids.append(matches[-1])
        return word_ids
            
    return do        
    